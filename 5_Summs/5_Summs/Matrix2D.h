#pragma once
#include <stdio.h>
#include <stdlib.h>

class Matrix2D {
  int **matrix;
  size_t nRows;
  size_t nColumns;
public:
  Matrix2D(size_t nRows, size_t nColumns);
  ~Matrix2D(void);
  void sumPerRow(int *result);
  void sumPerRow(int *result, size_t startIndex, size_t untilIndex);
  void sumPerColumn(int *result);
  void sumPerColumn(int *result, size_t startIndex, size_t untilIndex);
  size_t getColumnsCount();
  size_t getRowsCount();
  void show();
  static void zeroArray(int *value, size_t n);
  static void zeroMatrix(int **value, size_t nRows, size_t nColumns);
  static void createMatrix(int** &value, size_t nRows, size_t nColumns);
  static void showArray(int *value, size_t size);
private:
  void fillWithRandomValues(int range);
};

