#include "Matrix2D.h"

Matrix2D::Matrix2D(size_t n, size_t m):
  nRows(n),
  nColumns(m)
{
  matrix = new int*[nRows];
  for (size_t i = 0; i < nRows; i++) {
    matrix[i] = new int[nColumns];
  }
//  fillWithRandomValues(1 << 15);
  fillWithRandomValues(1 << 4);
}

void Matrix2D::sumPerRow(int *result) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      result[i] += matrix[i][j];
    }
  }
}

void Matrix2D::sumPerColumn(int *result) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0; j < nColumns; j++) {
      result[j] += matrix[i][j];
    }
  }
} 

Matrix2D::~Matrix2D(void) {
  for (size_t i = 0; i < nRows; i++) {
    delete [] matrix[i];
  }
  delete matrix;
}

void Matrix2D::fillWithRandomValues(int range) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      matrix[i][j] = 1;//(rand() % (2 * range) - range);
    }
  }
}

size_t Matrix2D::getColumnsCount() {
  return nColumns;
}

size_t Matrix2D::getRowsCount() {
  return nRows;
}

void Matrix2D::show() {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      printf("%d ", matrix[i][j]);
    }
    printf("\n");
  }
}

void Matrix2D::sumPerRow(int *result, size_t startIndex, size_t untilIndex) {
  for (size_t i = startIndex; i < untilIndex; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      result[i] += matrix[i][j];
    }
  }
}

void Matrix2D::sumPerColumn(int *result, size_t startIndex, size_t untilIndex) {
  for (size_t i = startIndex; i < untilIndex; i++) {
    for (size_t j = 0; j < nColumns; j++) {
      result[j] += matrix[i][j];
    }
  }
}

void Matrix2D::zeroArray(int *value, size_t n) {
  for (size_t i = 0; i < n; i++) {
    value[i] = 0;
  }
}

void Matrix2D::zeroMatrix(int **value, size_t nRows, size_t nColumns) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      value[i][j] = 0;
    }
  }
}

void Matrix2D::createMatrix(int** &value, size_t nRows, size_t nColumns) {
  value = new int*[nRows];
  for (size_t i = 0; i < nRows; i++) {
    value[i] = new int[nColumns];
  }
}

void Matrix2D::showArray(int *value, size_t size) {
  printf("\n");
  for (size_t i = 0; i < size; i++) {
    printf("%d ", value[i]);
  }
  printf("\n");
}
