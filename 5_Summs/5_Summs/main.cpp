#include <windows.h>
#include "Matrix2D.h"
#include "GetLastError.h"

/*
System Type:               X86-based PC
Processor(s):              1 Processor(s) Installed.
                           [01]: x64 Family 6 Model 28 Stepping 10 GenuineIntel ~1667 Mhz
1 2 4 8
*/

#include <windows.h>
#include <strsafe.h>

#define MAX_THREADS 2
#define MATRIX_SIZE (1 << 10)

DWORD WINAPI sumPerRow( LPVOID lpParam );
DWORD WINAPI sumPerColumn( LPVOID lpParam );

typedef struct MyData {
  Matrix2D *matrix;
  int *result;
  size_t val1;
  size_t val2;

  MyData () {
    result = new int[MATRIX_SIZE];
    for (size_t i = 0; i < MATRIX_SIZE; i++) {
      result[i] = 0;
    }
  }

  ~MyData () {
    delete[] result;
  }
} MYDATA, *PMYDATA;

void onMainThread();
void onSecondaryThreads(BOOL b);

int main(int argc, char **argv) {
  printf("\nMain thread");
  onMainThread();

  printf("\nSecondary threads\n");
  onSecondaryThreads(0);
  onSecondaryThreads(1);

  system("pause");
  return 0;
}

void onSecondaryThreads(BOOL b) {
  
  Matrix2D matrix(MATRIX_SIZE, MATRIX_SIZE);
  PMYDATA pDataArray[MAX_THREADS];
  DWORD   dwThreadIdArray[MAX_THREADS];
  HANDLE  hThreadArray[MAX_THREADS];
  
  for(size_t i = 0; i < MAX_THREADS; i++) {
    pDataArray[i] = new MyData;
    pDataArray[i]->matrix = &matrix;
    pDataArray[i]->val1 = i * matrix.getRowsCount() / MAX_THREADS;
    pDataArray[i]->val2 = (i + 1) * matrix.getRowsCount() / MAX_THREADS;
  }

  DWORD firstValue = GetTickCount();
  for(size_t i = 0; i < MAX_THREADS; i++) {
    hThreadArray[i] = CreateThread( 
        NULL,                   // default security attributes
        0,                      // use default stack size  
        (b ? sumPerColumn : sumPerRow),              // thread function name
        pDataArray[i],          // argument to thread function 
        0,                      // use default creation flags 
        &dwThreadIdArray[i]);   // returns the thread identifier 
    if (NULL == hThreadArray[i])  {
      ErrorExit("CreateThread");
      return;
    }
  }

  WaitForMultipleObjects(MAX_THREADS, hThreadArray, TRUE, INFINITE);
  printf("\ntime : ");
  printf((b ? "%d - per columns\n" : "%d - per rows\n"), GetTickCount() - firstValue);

  int *result = new int[MATRIX_SIZE];
  Matrix2D::zeroArray(result, MATRIX_SIZE);
  for (size_t i = 0; i < MAX_THREADS; i++) {
    for (size_t j = 0; j < matrix.getColumnsCount(); j++) {
      result[j] += pDataArray[i]->result[j];
    }
  }

  Matrix2D::showArray(result, 10/*matrix.getColumnsCount()*/);
  
  delete [] result;

  for(size_t i = 0; i < MAX_THREADS; i++) {
    CloseHandle(hThreadArray[i]);
    delete pDataArray[i];
  }
}

DWORD WINAPI sumPerRow(LPVOID lpParam) { 

  PMYDATA pData;
  pData = (PMYDATA)lpParam;
  (pData->matrix)->sumPerRow(pData->result, pData->val1, pData->val2);

  return 0; 
}

DWORD WINAPI sumPerColumn(LPVOID lpParam) { 

  PMYDATA pData;
  pData = (PMYDATA)lpParam;
  (pData->matrix)->sumPerColumn(pData->result, pData->val1, pData->val2);

  return 0; 
}

void onMainThread() {
  Matrix2D m(MATRIX_SIZE, MATRIX_SIZE);
  
  int *perRows = new int[m.getRowsCount()];
  int *perColumns = new int[m.getColumnsCount()];
  Matrix2D::zeroArray(perColumns, m.getColumnsCount());
  Matrix2D::zeroArray(perRows, m.getRowsCount());

  // per rows
  DWORD firstValue = GetTickCount();
  m.sumPerRow(perRows);
  printf("\ntime : %d - per rows", GetTickCount() - firstValue);
  Matrix2D::showArray(perRows, 10/*m.getRowsCount()*/);
  printf("\n");

  // per columns
  firstValue = GetTickCount();
  m.sumPerColumn(perColumns);
  printf("\ntime : %d - per columns ", GetTickCount() - firstValue);
  Matrix2D::showArray(perColumns, 10/*m.getColumnsCount()*/);
  printf("\n");

  delete perRows;
  delete perColumns;  
}