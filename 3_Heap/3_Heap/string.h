#pragma once
#include <iostream>
class string
{
public:

	string(void): t(0),size(0){}
	string(const char * q){
		if(q){
			size = strlen(q);
			
			t = new	char[size + 1];
			strcpy(t,q);

		} else { t = 0; size = 0; }
	}

	string(const string& q){
		if(q.size && q.t){
			size = q.size;
			t = new	char[size + 1];
			strcpy(t,q.t);
		} else { t = 0; size = 0; }
	}

	friend std::ostream& operator << (std::ostream& out,string p){
		if(p.t)
		out << p.t << '\n';
		return out;
	}
	string& operator = (const string& q){
		if(this != &q)
		{
			delete [] t;
			if(q.size && q.t)
			{
				size = q.size;
				t = new	char[size + 1];
				strcpy(t,q.t);
			} else {
				t = 0; size = 0;
			}
		}
		return *this;
	}
	int Length(){
		return size;
	}

	~string(void){
		delete [] t;	
		size = 0;
	}

private:

	int size;
	char * t;
};

