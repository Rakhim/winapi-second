#pragma once
#include "cabcfurn.h"
class WarCabCfurn :
	public CabCfurn
{
public:

	virtual furn * Clone() {
		return new WarCabCfurn(*this);
	}

	WarCabCfurn(void): CabCfurn() {}
	WarCabCfurn(const char * q): CabCfurn(q) {}
	WarCabCfurn(WarCabCfurn& q) {
		ps = q.ps;
	}
	~WarCabCfurn(void){}
};

