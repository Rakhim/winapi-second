#pragma once
#include <windows.h>

struct HEAP_EXCEPTION {
  char *message;
};

class Heap {
public:
  Heap(SIZE_T dwInitialSize = 0, SIZE_T dwmaximumSize = 0);
  ~Heap(void);
  LPVOID alloc(SIZE_T dwNumberOfBytes);
  BOOL free(LPVOID lpAddress);
private:
  HANDLE hHeap;
  
};

