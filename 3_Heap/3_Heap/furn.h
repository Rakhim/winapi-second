#pragma once
#include "string.h"
//#include <Windows.h>
#include "Heap.h"

class furn {
//  static HANDLE hHeap;
  static Heap *pHeap;
  static SIZE_T countOfInstances;
protected:
	string ps;
public:
	furn() {
  	ps;
  }

	furn(const char* q) {
		ps = q;
	}

	furn(const furn& q) {
		ps = q.ps;
	}

	virtual furn* Clone() = 0;

	virtual string& Name() {
  	return ps;

  };

	virtual ~furn() { };
  
  LPVOID operator new  (size_t n) {
    if (!countOfInstances) {
      pHeap = new Heap();
    }
    ++countOfInstances;
    return pHeap->alloc(n);
  };

  VOID operator delete (LPVOID lpAddress) {
    if (NULL == lpAddress) return;

    --countOfInstances;
    pHeap->free(lpAddress);
    if (!countOfInstances) {
      delete pHeap;
    }
  }
};
