#pragma once
#include "laysfurn.h"
class SofaLaySfurn :
	public LaySfurn
{
public:

	virtual furn * Clone()
	{
		return new SofaLaySfurn(*this);
	}

	SofaLaySfurn(void): LaySfurn() {}
	SofaLaySfurn(const char * q): LaySfurn(q) {}
	SofaLaySfurn(SofaLaySfurn& q){
		ps = q.ps;
	}
	~SofaLaySfurn(void){}
};

