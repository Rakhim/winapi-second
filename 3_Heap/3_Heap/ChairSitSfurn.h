#pragma once
#include "sitsfurn.h"
class ChairSitSfurn :
	public SitSfurn
{
public:

	virtual furn * Clone()
	{
		return new ChairSitSfurn(*this);
	}

	ChairSitSfurn(void): SitSfurn() {}
	ChairSitSfurn(const char * q): SitSfurn(q) {}
	ChairSitSfurn(ChairSitSfurn& q) {
		ps = q.ps;
	}
	~ChairSitSfurn(void){}
};

