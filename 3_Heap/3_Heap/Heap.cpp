#include "Heap.h"


Heap::Heap(SIZE_T dwInitialSize, SIZE_T dwmaximumSize) {
  hHeap = HeapCreate(HEAP_NO_SERIALIZE, dwInitialSize, dwmaximumSize);
  if (NULL == hHeap) {
    HEAP_EXCEPTION e;
    e.message = "HEAP : constructor ";
    throw e;
  }
}

Heap::~Heap(void) {
  if(NULL == HeapDestroy(hHeap)) {
    HEAP_EXCEPTION e;
    e.message = "HEAP : destructor ";
    throw e;
  }
}

LPVOID Heap::alloc(SIZE_T dwNumberOfBytes) {
  return HeapAlloc(hHeap, HEAP_ZERO_MEMORY, dwNumberOfBytes);
}

BOOL Heap::free(LPVOID lpAddress) {
  return HeapFree(hHeap, HEAP_NO_SERIALIZE, lpAddress);  
}
