#pragma once
#include "cabcfurn.h"
class BookCabCfurn :
	public CabCfurn
{
public:	

	virtual furn * Clone()
	{
		return new BookCabCfurn(*this);
	}

	BookCabCfurn(void): CabCfurn() {}
	BookCabCfurn(const char * q): CabCfurn(q) {}
	
	
	BookCabCfurn(BookCabCfurn& q)
	{
		ps = q.ps;
	}
	~BookCabCfurn(void){}
};

