#include <windows.h>
#include <iostream>

DWORD WINAPI threadFunction(LPVOID lpParam );

int main(int argc, char **argv) {
  const DWORD nThreads = 156;
  HANDLE hThreads[nThreads];
  SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);

  for (size_t i = 0; i < nThreads; i++) {
    hThreads[i] = CreateThread(NULL, 0, threadFunction, NULL, CREATE_SUSPENDED, NULL);
  }

  for (size_t i = 0; i < nThreads; i++) {
    SetThreadPriority(hThreads[i], THREAD_PRIORITY_LOWEST);
  }

  for (size_t i = 0; i < nThreads; i++) {
    ResumeThread(hThreads[i]);
  }
  WaitForMultipleObjects(nThreads, hThreads, TRUE, INFINITE);
  
  for (size_t i = 0; i < 100; i++) {
    printf(" main"); // Main thread almost forever will processed first 
  }
  
  system("pause");
  return 0;
}

DWORD WINAPI threadFunction( LPVOID lpParam ) {
  printf(" secondary"); // // Secondary threads almost forever will processed last 
  return 0;
}