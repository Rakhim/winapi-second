#pragma once
#include <Windows.h>
//#include "ListShape.h"
#include "Printable.h"
class Offscreen {
  HDC hdcCompatible;
  HBITMAP hBitmap;
  HBITMAP hBitmapOld;
  
public:
  void Offscreen::write(Printable *pPrintable);
  //void Offscreen::write(ListShape *listShape);

  void Offscreen::print(HDC hdc, LPRECT r);

  Offscreen(HDC hdc, int cx, int cy);
  ~Offscreen(void);
};

