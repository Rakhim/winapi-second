// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // ��������� ����� ������������ ���������� �� ���������� Windows
// ����� ���������� Windows:
#include <windows.h>

// ����� ���������� C RunTime
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
#include "CircleShape.h"
#include "RectangleShape.h"
#include "TriangleShape.h"
#include "shape.h"
#include "ListShape.h"
#include "GetLastError.h"
#include "Tools.h"
#include "Offscreen.h"