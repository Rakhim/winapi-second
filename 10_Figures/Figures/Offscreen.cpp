#include "StdAfx.h"
#include "Offscreen.h"


Offscreen::Offscreen(HDC hdc, int cx, int cy) {
  hdcCompatible = CreateCompatibleDC(hdc);
  hBitmap = CreateCompatibleBitmap(hdc, cx, cy);
  hBitmapOld = (HBITMAP) SelectObject(hdcCompatible, hBitmap);
  PatBlt(hdcCompatible, 0, 0, cx, cy, WHITENESS);
}


Offscreen::~Offscreen(void) {
  DeleteDC(hdcCompatible);
  DeleteObject(hBitmap);
}

void Offscreen::write(Printable *pPrintable) {
  pPrintable->print(hdcCompatible);
}

void Offscreen::print(HDC hdc, LPRECT r) {
	BitBlt(hdc, r->left, r->top, r->right, r->bottom, hdcCompatible, 0, 0, SRCCOPY);
}