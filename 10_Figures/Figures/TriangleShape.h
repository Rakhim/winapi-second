#pragma once
#include "shape.h"
class TriangleShape :
	public Shape
{
private:
	COLORREF crBurd;
	COLORREF crIn;
	POINT ps[3];

public:
	TriangleShape(void): crBurd(0),crIn(0)
	{
		ps[2].x = ps[2].y = 0;
		ps[0] = ps[1] = ps[2];
	}

	TriangleShape(POINT pOne,POINT pTwo,POINT pThree,COLORREF crBurder,COLORREF crInterface):  crBurd(crBurder),crIn(crInterface)
	{
/**/		ps[0] = pOne;
			ps[1] = pTwo;
			ps[2] = pThree;
	}

	TriangleShape(const TriangleShape& p)
	{
		for(int i = 0;i < 3;i++)
			ps[i] = p.ps[i];
			
		crBurd = p.crBurd;
		crIn = p.crIn;
	}

	virtual Shape* Clone()
	{
		return new TriangleShape(*this);
	}

	virtual void print(HDC& hdc)
	{
		HPEN hpen,hpenOld;
		HBRUSH hbrush,hbrushOld;
			
		hpen = CreatePen(PS_DASHDOT, 1, crBurd);
		hbrush = CreateSolidBrush(crIn);
						
		hpenOld = (HPEN)SelectObject(hdc, hpen);
		hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);

		Polygon(hdc, ps, 3);

		SelectObject(hdc, hpenOld);
		SelectObject(hdc, hbrushOld);
		
		DeleteObject(hbrush);
		DeleteObject(hpen);
	}


	~TriangleShape(void){};
};

