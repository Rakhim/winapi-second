#pragma once
#include "shape.h"
class CircleShape :
	public Shape
{
private:
	COLORREF crBurd;
	COLORREF crIn;
	int x,y,R;
public:
	CircleShape(void);

	CircleShape(const CircleShape& p)
	{
		crBurd = p.crBurd;
		crIn = p.crIn;
		x = p.x;
		y = p.y;
		R = p.R;
	}
	
	CircleShape(POINT O,int Radius,COLORREF crBurder): crBurd(crBurder),crIn(crBurder),x(O.x),y(O.y),R(Radius){}
	CircleShape(POINT O,int Radius,COLORREF crBurder,COLORREF crInterface): crBurd(crBurder),crIn(crInterface),x(O.x),y(O.y),R(Radius){}
	
	virtual void print(HDC& hdc)
	{
		HPEN hpen,hpenOld;
		HBRUSH hbrush,hbrushOld;
				
			hpen = CreatePen(PS_SOLID, 1, crBurd);
			hbrush = CreateSolidBrush(crIn);

			hpenOld = (HPEN)SelectObject(hdc,hpen);
			hbrushOld = (HBRUSH)SelectObject(hdc,hbrush);

				Ellipse(hdc,x - R,y - R,x + R,y + R);
		
			SelectObject(hdc,hpenOld);
			SelectObject(hdc,hbrushOld);

		DeleteObject(hbrush);
		DeleteObject(hpen);
	}

	virtual Shape* Clone()
	{
		return new CircleShape(*this);
	}

	~CircleShape(void);
};

