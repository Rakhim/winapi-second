#pragma once
#include "shape.h"
#include <iostream>
#include <string.h>
#include "Printable.h"

class ListShape:
	public Printable
{
private:
	struct Node{
		Node * next;
		Shape* x;
		Node(): next(0) ,x(0) {}
		Node(Shape* p)
		{
			x = p->Clone();
			next = 0;
		}
	}* head,* tail;

private:
 	void AddH(Shape* q);
	void AddT(Shape*& q);
	void DelH(Node * & ph);

public:
//	virtual shape* Clone();
	ListShape();
	ListShape(const ListShape&);
	void Push(Shape*);
	void PushBack(Shape*);
	void Del(void);
	void Clear();
	virtual void print(HDC&);
/*----------------------------------------------*/	
	ListShape& operator = (const ListShape& q)
	{
		if(this != &q)
		{
			while(head){
				DelH(head);
			}

			Node * ps = q.head;

			while(ps){
				AddT(ps->x);
				ps = ps->next;
			}
		}
		return *this;
	}

	~ListShape()
	{
		while(head){
			DelH(head);
		}
		head = tail = 0;
	}
};
