// Program.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Program.h"
#include <string.h>
#include "CircleShape.h"
#include "TriangleShape.h"
#include "RectangleShape.h"
#include "ListShape.h"

#include <iostream>

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <fstream>

#include <CommDlg.h>
#include <CommCtrl.h>

#include <list>

using namespace std;


static const WORD MAX_CONSOLE_LINES = 500;

#define MAX_LOADSTRING 100

#define NUMLINES ((int)(sizeof sysmetrics / sizeof sysmetrics [0]))

#define IDL_BUTTON 21000
#define IDL_EDIT 21001
#define IDL_COMBOBOX 21002
#define IDL_LISTBOX 21003
#define IDL_BUTTONONE 31000
#define IDM_TOOLBAR 31001
#define IDM_STATUS  31002



// ���������� ����������:
HINSTANCE hInst;								// ������� ���������
TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: ���������� ��� �����.
	MSG msg;
	HACCEL hAccelTable;

	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PROGRAM, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROGRAM));

	// ���� ��������� ���������:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_DBLCLKS;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra	= 0;
	wcex.cbWndExtra	= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROGRAM));
	wcex.hCursor		= LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CURSOR2));
	wcex.hbrBackground= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_PROGRAM);
	wcex.lpszClassName= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;
	
   hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW|WS_VSCROLL|WS_HSCROLL|WS_EX_LAYERED,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL); 

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

COLORREF crRand()
{
	 return RGB(rand()%256,rand()%256,rand()%256);
}

POINT PointRand(POINT pDOT,int Radius = 0,HWND hWn = NULL)
{
	POINT res = pDOT;

	if((Radius > 0) && !hWn)
	{
		res.x += rand()%(2*Radius) - Radius;
		res.y += rand()%(2*Radius) - Radius;
	} else
	if(hWn != NULL)
	{
		RECT rectTemp;
		GetClientRect(hWn,&rectTemp);

		res.x = rand()%rectTemp.right;
		res.y = rand()%rectTemp.bottom;
	}
	return res;
}

void DrawBoxOutline(HWND hwnd, POINT ptBeg, POINT ptEnd)
{
	HDC hdc;
	hdc = GetDC(hwnd);
		SetROP2(hdc, R2_NOTXORPEN);
		HGDIOBJ hOld = GetStockObject(ANSI_FIXED_FONT);

		SelectObject(hdc, hOld);
		Rectangle(hdc,ptBeg.x,ptBeg.y,ptEnd.x,ptEnd.y);

	DeleteObject(hOld);
	ReleaseDC(hwnd, hdc);
}

void SetMode(HDC hDc,int cxClient,int cyClient,int xOrigin = 0,int yOrigin = 0)
{
//		SetMapMode(hDc, MM_ANISOTROPIC);
//		SetWindowExtEx(hDc,676, 676, NULL);
//		SetViewportExtEx(hDc, cxClient, cyClient, NULL);
		SetViewportOrgEx(hDc, xOrigin, yOrigin, NULL);
}
void FillListRandom(ListShape* &lsp,int N,int loc,HWND hWnd)
{
	lsp = new ListShape();
	shape * shp;

	for(int i = 0;i < N;i++)
	{
		POINT pDOT = {0,0};
				pDOT = PointRand(pDOT,0,hWnd);
		COLORREF cBurder  = crRand();
		COLORREF cInterface = crRand();

		switch(rand()%3)
		{
			case 0:
			{
				shp = new CircleShape(pDOT,rand()%(loc/3),cBurder,cInterface);
				lsp->Push(shp);
				delete shp;
			}
			break;
			case 1:
			{
				shp = new TriangleShape(pDOT,PointRand(pDOT,loc),PointRand(pDOT,loc),cBurder,cInterface);
				lsp->Push(shp);
				delete shp;
			}
			break;
			case 2:
			{
				shp = new RectangleShape(pDOT,PointRand(pDOT,loc),cBurder,cInterface);
				lsp->Push(shp);
				delete shp;
			}
			break;
		}
	}
}

void WINAPI CheckShapeItem(HWND hwnd, UINT uID) 
{ 
    HMENU hmenuBar = GetMenu(hwnd); 
	 	 	 
    CheckMenuRadioItem( 
            hmenuBar,         // handle to menu 
            IDM_TRIANGLE,           // first item in range 
            IDM_CIRCLE,          // last item in range 
            uID,                // item to check 
            MF_BYCOMMAND        // IDs, not positions 
            ); 
} 

INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Dialog(HWND, UINT, WPARAM, LPARAM);

// Example begin
	HIMAGELIST g_hImageList = NULL;

HWND CreateSimpleToolbar(HWND hWndParent)
{
    // Declare and initialize local constants.
    const int ImageListID    = 0;
    const int numButtons     = 3;
    const int bitmapSize     = 16;
    
    const DWORD buttonStyles = BTNS_AUTOSIZE;
    
    
    TBBUTTON tbButtons[numButtons] = 
    {
        { 0, IDM_CIRCLE,  TBSTATE_ENABLED, buttonStyles, {0}, 0,0/* (INT_PTR)L"" */},
        { 1, IDM_TRIANGLE, TBSTATE_ENABLED, buttonStyles, {0}, 0,0/* (INT_PTR)L""*/},
        { 2, IDM_RECTANGLE, TBSTATE_ENABLED, buttonStyles, {0}, 0,0/* (INT_PTR)L""*/}
    };

	 // Create the toolbar.
    HWND hWndToolbar = CreateToolbarEx(hWndParent,WS_CHILD,IDM_TOOLBAR,1,hInst,IDB_TOOLBAR,
													tbButtons,3,24,24,20,20,sizeof(TBBUTTON));
		
	 ShowWindow(hWndToolbar,  TRUE);
    
    return hWndToolbar;
}
//   The handle to the status bar.
//
HWND DoCreateStatusBar(HWND hwndParent)
{
	HWND hStatus = CreateStatusWindow(WS_CHILD|WS_VISIBLE,L"Ahhha",hwndParent,IDM_STATUS);
   return hStatus;
}  

//=======================================================================
/*
HWND CreateListView (HWND hwndParent) 
{
    INITCOMMONCONTROLSEX icex;           // Structure for control initialization.
    icex.dwICC = ICC_LISTVIEW_CLASSES;
    InitCommonControlsEx(&icex);

    RECT rcClient;                       // The parent window's client area.

    GetClientRect (hwndParent, &rcClient); 

    // Create the list-view window in report view with label editing enabled.
    HWND hWndListView = CreateWindow(WC_LISTVIEW, 
                                     L"LIST VIEW",
                                     WS_CHILD | LVS_REPORT | LVS_EDITLABELS,
                                     0, 0,
                                     rcClient.right - rcClient.left,
                                     rcClient.bottom - rcClient.top,
                                     hwndParent,
                                     (HMENU)IDC_PROGRAM,//M_CODE_SAMPLES,
                                     hInst,
                                     NULL); 

    return (hWndListView);
}
*/
//=======================================================================



//-----------------------------------------------------------------------
/* Status Bar*/
void ScrollInit(HWND hWn)
{
	SCROLLINFO siInfo;
		siInfo.cbSize =  sizeof(SCROLLINFO);
		siInfo.fMask = SIF_ALL;//|SIF_PAGE;
		siInfo.nMin =  0;
		siInfo.nMax = 1000;
		siInfo.nPage = 200;
		siInfo.nPos = 0;
		siInfo.nTrackPos = 0;

	SetScrollInfo(hWn,SB_VERT,&siInfo,FALSE);	
									 
	siInfo.nMax = 1500;

	SetScrollInfo(hWn,SB_HORZ,&siInfo,FALSE);
}

void TableInit(HWND &hwEdit,HWND &hwCombo,HWND &hwButton,HWND &hwList,HWND hWnd)
{
		{
			int x = 30,y = 40;
			int a = 90,b = 25,d = 7;

					hwEdit = CreateWindow(WC_EDIT, TEXT("������� "), CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
						x , y, a, b, hWnd, (HMENU)IDL_EDIT, hInst, NULL);

					hwCombo = CreateWindow(WC_COMBOBOX, TEXT(" .."), CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
						x + a + d, y, a, 3*b , hWnd,  (HMENU)IDL_COMBOBOX, hInst, NULL);
							
					hwButton = CreateWindow(WC_BUTTON, TEXT(" �� ?"),/* CBS_DROPDOWN | /*CBS_HASSTRINGS |*/ WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
						x + 2*a + 2*d, y, a, b , hWnd, (HMENU)IDL_BUTTON, hInst, NULL);

					hwList = CreateWindow(WC_LISTBOX, TEXT(" .."), CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
						x , y + b + d, 3*a + 2*d, 2*b , hWnd,  (HMENU)IDL_LISTBOX, hInst, NULL);

					hwButton = CreateWindow(WC_BUTTON, TEXT(" ������� ����� "),/* CBS_DROPDOWN | /*CBS_HASSTRINGS |*/ WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
						x , y + b + d + 2*b, 3*a + 2*d , b , hWnd, (HMENU)IDL_BUTTONONE, hInst, NULL);

		}

		
		TCHAR Planets[4][14] =  
		{
				TEXT("����������"), TEXT("�������������"), TEXT("�����������"),TEXT("��� ������ ")	
		};
       
		TCHAR A[16]; 

	memset(&A,0,sizeof(A));       
	for (int k = 0; k <= 3; k += 1)
	{
			wcscpy_s(A, sizeof(A)/sizeof(TCHAR),  (TCHAR*)Planets[k]);
			SendMessage(hwCombo,(UINT) CB_ADDSTRING,(WPARAM) 0,(LPARAM) A); 
	}
	SendMessage(hwCombo, CB_SETCURSEL, (WPARAM)2, (LPARAM)0);

}		


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
//	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc, hCompatibleDC;;
	static ListShape* lsp;
	
	static int cxClient,cyClient;
	static POINT Cursor;
	int loc = 60;
	static int cxChar, cyChar = 1000, iVscrollPos = 0,iHscrollPos = 0;
	static int shape_mode;

	static BOOL fBlocking;
	static POINT ptBeg;

	static ofstream temp;//("temp.txt");

	static HANDLE hBitmap, hOldBitmap;
	static BITMAP Bitmap;

	static HWND hwEdit;
	static HWND hwCombo;
	static HWND hwList;  
	static HWND hwButton;
	static HWND hStatusBar;

	static RECT rect;

	static TCHAR szFile[256];

	static HDC hdcOffscreen;
	
	static HBITMAP hBmpOffscreen, hBmpOld;

	static DWORD rgbCurrent = 0x00FF;        // initial color selection
	static HFONT hfont;
	static HWND hStatus;

	static HWND hNext;

/*
	 open safe
	 getOpenFilename
	 getColorChoose
	 ChooseFont
*/
	
	switch (message)
	{
//		case WM_ERASEBKGND:
//			return TRUE;

	  /* case WM_CTLCOLORLISTBOX:
            SetTextColor( (HDC)wParam, RGB( 0, 255, 0 ) );
				SetBkColor( (HDC)wParam, RGB(0,0,255));
            return (LRESULT)GetStockObject( BLACK_BRUSH );
				*/
		case WM_CREATE:
			cout << "\nWM_CREATE";
			{		
				HDC hdc = GetDC(hWnd);
					
					hNext = SetClipboardViewer(hWnd);
					// -------------------------------------------

				//	SetClipboardData(CF_BITMAP, hdc);


					//HWND hWn = CreateWindow(szWindowClass, szTitle, WS_CHILD,
				   //   CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, hWnd, NULL, hInst, NULL); 
				
					hdcOffscreen = CreateCompatibleDC(hdc);
					//hStatusBar = DoCreateStatusBar(hWnd,0,hInst,1);
					CreateSimpleToolbar(hWnd);

					//HWND hList = CreateListView(hWnd);
					
					hStatus = DoCreateStatusBar(hWnd);

					int cx = GetSystemMetrics(SM_CXSCREEN);
					int cy = GetSystemMetrics(SM_CYSCREEN);

					hBmpOffscreen = CreateCompatibleBitmap(hdc,cx, cy);
					hBmpOld = (HBITMAP) SelectObject(hdcOffscreen, hBmpOffscreen);
					PatBlt(hdcOffscreen,0,0, cx, cy, WHITENESS);

//					HWND hWCHild = CreateWindow(

					/* offscreen begin*/

						shape_mode = IDM_TRIANGLE;
						CheckShapeItem(hWnd,shape_mode);		
						// for stastus bar
						SendMessage(hStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)L"����������� ");
						//RedirectIOToConsole();


						hBitmap = LoadImage(hInst, L"picture.bmp"/*szFile*/, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
						TableInit( hwEdit, hwCombo, hwButton, hwList, hWnd);
						ScrollInit(hWnd);
						
						lsp = new ListShape();
						FillListRandom(lsp,20,60,hWnd);
						lsp->Print(hdcOffscreen,0);

						//CreateSimpleToolbar(hWnd);

					/* offscreen end*/
				ReleaseDC(hWnd,hdc);
			}
		break;

		case WM_PAINT:
			{

				hdc = BeginPaint(hWnd, &ps);
				SetMode(hdc,cxClient,cyClient,iHscrollPos,iVscrollPos);
				
				//TextOut(hdcOffscreen, 10, 10, L"It does not matter how slowly you go so long as you do not stop.", _tcslen(szFile));

				RECT r;
				GetClientRect(hWnd,&r);
				BitBlt(hdc, 0, 0, r.right, r.bottom, hdcOffscreen,0,0,SRCCOPY);

				//InitListView(hWnd);

				EndPaint(hWnd, &ps);
			}
		break;	
		/*FOR CONTROLS . BEGIN*/

		case WM_NOTIFY:
			{
				std::cout << "\nNOTIFY";	
			}
			break;

	case WM_COMMAND:
			{
				switch(LOWORD(wParam))
				{				
					case IDL_BUTTON:
					{
						//std::cout << "\n IDL_BUTTON";
						switch(HIWORD(wParam))
						{
							case BN_CLICKED: 
								{
						//			std::cout << "\nBN_CLICKED";

									int size = 13;
									TCHAR * szBuffer = new TCHAR[size];
									((WORD*) szBuffer)[0] = size;
									
									 size = 13;
									if(size = SendMessage(hwEdit,EM_LINELENGTH,(WPARAM) 0,(LPARAM) 0))
									{
										TCHAR * szBuffer = new TCHAR[size];
										((WORD*) szBuffer)[0] = size;
										szBuffer[size] = 0;

										SendMessage(hwEdit, EM_GETLINE,(WPARAM) 0, (LPARAM) szBuffer);
										SendMessage(hwList, LB_ADDSTRING, 0, (LPARAM) szBuffer);
										SendMessage(hwEdit, EM_EMPTYUNDOBUFFER,0,0); 
									}

									// pointer to string

								}
								break;
							case BN_DISABLE:
								{
									std::cout << "\n BN_DISABLE";
								}
							break;
						}
					}
					break;

					case IDL_BUTTONONE:
					{
						//std::cout << "\n IDL_BUTTON";
						switch(HIWORD(wParam))
						{
							case BN_CLICKED: 
								{
									//hdc = GetDC(hWnd);           // display device context of owner window

										CHOOSEFONT cf;            // common dialog box structure
										static LOGFONT lf;        // logical font structure
										static DWORD rgbCurrent;  // current text color
										HFONT hfontPrev;
										DWORD rgbPrev;

										// Initialize CHOOSEFONT
										ZeroMemory(&cf, sizeof(cf));
										cf.lStructSize = sizeof (cf);
										cf.hwndOwner = hWnd;
										cf.lpLogFont = &lf;
										cf.rgbColors = rgbCurrent;
										cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

										if (ChooseFont(&cf)==TRUE)
										{
											 hfont = CreateFontIndirect(cf.lpLogFont);
											 hfontPrev =  (HFONT)SelectObject(hdcOffscreen, hfont);
											 DeleteObject(hfontPrev);

											 rgbCurrent= cf.rgbColors;
											 rgbPrev = SetTextColor(hdcOffscreen, rgbCurrent);
										}

											TextOut(hdcOffscreen, 10, 200, L"It does not matter how slowly you go so long as you do not stop.", 64);
											RECT r;
											r.left = 10;
											r.top = 200;
											r.right = 100;
											r.bottom = 20;
										InvalidateRect(hWnd, &r, TRUE);
										//ReleaseDC(hWnd,hdc);
								}
								break;
						}
					}
					break;


					case IDL_COMBOBOX:
					{
						std::cout << "\n IDL_COMBOBOX";
											InvalidateRect(hWnd,0,0);
					
						switch(HIWORD(wParam))
						{
							case CB_SELECTSTRING:
								{
										InvalidateRect(hWnd,0,0);
								}
							break;
							
						}
					}
					break;

					case IDL_EDIT:
					{
						std::cout << "\n IDL_EDIT";
						switch(HIWORD(wParam))
						{
							case EM_GETLINE: 
								{
										std::cout << "\nEM_GETLINE";
										//SendMessage(hWnd,EM_GETLINE,0,0);

								}
								break;
							case EM_GETHANDLE: 
								{
										std::cout << "\nEM_GETLINE";
										//SendMessage(hWnd,EM_GETLINE,0,0);
								}
								break;
						}
					}
					break;

					case IDL_LISTBOX:
					{
						std::cout << "\n IDL_LISTBOX";
						//LB_ADDSTRING
						switch(HIWORD(wParam))
						{
								case LB_ADDSTRING:
									{
										std::cout << "\n  Hello, World ! ";
									}
									break;
						}
					}
					break;

				/* Further all the old */
			case IDM_CIRCLE:
				{		
//					shape_mode = IDM_CIRCLE; 
//					SendMessage(hStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)L"���������� ");

						hdc = GetDC(hWnd);

						BITMAP bm;
				//		HGDIOBJ obj = GetCurrentObject(hdc,OBJ_BITMAP);
						GetObject(hdc, sizeof(bm), &bm);
						
						RECT r;
							GetClientRect(hWnd,&r);

						HBITMAP chBitmap = CreateCompatibleBitmap(hdc,r.right,r.bottom);//bm.bmWidth, bm.bmHeight);

						HDC hcDC = CreateCompatibleDC(hdc);
						
						HBITMAP hbm = (HBITMAP)SelectObject(hcDC,chBitmap);
						

						//	BITMAP bm;

						//GetObject(hBitmap,sizeof(HBITMAP),&bm);

							BitBlt(hcDC, 0,0, r.right, r.bottom, hdc, 0,0, SRCCOPY);
							SelectObject(hcDC,hbm);
							  
						  OpenClipboard(hWnd);
						  EmptyClipboard();
						
//						if(chBitmap != NULL)
						{
//							std::cout << "\n IIIIIIIIIIIIIIIIIIIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOooo";
							SetClipboardData(CF_BITMAP,chBitmap);
						}
						CloseClipboard();

					ReleaseDC(hWnd,hdc);
				}
				break;

			case IDM_TRIANGLE:
				{			 
					shape_mode = IDM_TRIANGLE;
					SendMessage(hStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)L"����������� ");
				}
				break;

			case IDM_RECTANGLE:
				{

//					shape_mode = IDM_RECTANGLE;		
//					SendMessage(hStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)L"������������� ");
					//[wlegmpderb

//					CreateCompatibleBitmap((HDC)GetClipboardData(CF_BITMAP),r.right,r.bottom);
//					(HBITMAP) SelectObject(hdcOffscreen, CreateCompatibleBitmap((HDC)GetClipboardData(CF_BITMAP),r.right,r.bottom));
//					PatBlt(hdc,0,0, r.right,r.bottom, WHITENESS);

//					InvalidateRect(hWnd,0,0);
//					ReleaseDC(hWnd,hdc);
					//MessageBox(hWnd,(LPCWSTR)GetClipboardData(CF_TEXT),L"!!!!",0);
					
						hdc = GetDC(hWnd);

						OpenClipboard(hWnd);
						hBitmap = (HBITMAP)GetClipboardData(CF_BITMAP);
						if(hBitmap != NULL)
						{
							HDC hcDC = CreateCompatibleDC(hdc);
							SelectObject(hcDC,hBitmap);
							BITMAP bm;
							RECT r;
							GetClientRect(hWnd,&r);

							GetObject(hBitmap,sizeof(HBITMAP),&bm);
							BitBlt(hdc, 0,0, r.right, r.bottom, hcDC, 0,0, SRCCOPY);
							std::cout << "\n OGOGGOGOGOGOGOGOGOGOGOGOGOGOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo";
						}					
						CloseClipboard();	
						
					//InvalidateRect(hWnd,0,0);
					ReleaseDC(hWnd,hdc);
				}
				break;

			case IDM_OPEN:
				{
					OPENFILENAME ofn;       // common dialog box structure
					//LPWSTR szFile = new WCHAR[256];       // buffer for file name
//					HWND hwnd;              // owner window
					HANDLE hf;            // file handle

					// Initialize OPENFILENAME
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = hWnd;
				ofn.lpstrFile = szFile;
						// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
						// use the contents of szFile to initialize itself.
				ofn.lpstrFile[0] = '\0';
				ofn.nMaxFile = sizeof(szFile);
				ofn.lpstrFilter = L"��� �����\0*.*\0�����������(.bmp)\0*.bmp\0";						
				ofn.nFilterIndex = 1;
				ofn.lpstrFileTitle = NULL;
				ofn.nMaxFileTitle = 0;
				ofn.lpstrInitialDir = NULL;
				ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

				// Display the Open dialog box.
				if ((GetOpenFileName(&ofn)) == TRUE)
					 hf = CreateFile(ofn.lpstrFile, GENERIC_READ, 0, (LPSECURITY_ATTRIBUTES) NULL,
						OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL);
						

						HANDLE hbTemp = LoadImage( hInst, ofn.lpstrFile, IMAGE_BITMAP, 500, 500, LR_LOADFROMFILE);

						InvalidateRect(hWnd,0,0);

						GetObject(hBitmap, sizeof(BITMAP), &Bitmap);
 						hCompatibleDC = CreateCompatibleDC(hdc);
 						hOldBitmap = SelectObject(hCompatibleDC, hBitmap);
 						GetClientRect(hWnd, &rect);
 
 						//StretchBlt(hdc, 0, 0, Rect.right, Rect.bottom, hCompatibleDC, 0, 0, Bitmap.bmWidth, Bitmap.bmHeight, SRCCOPY);
 
 			 			BitBlt( hdcOffscreen, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hCompatibleDC, 0, 0, SRCCOPY );
 
 						SelectObject(hCompatibleDC, hOldBitmap);
 						DeleteDC(hCompatibleDC);
				}
				break;

			case IDM_COLOR:
				{
						CHOOSECOLOR cc;                 // common dialog box structure 
						static COLORREF acrCustClr[16]; // array of custom colors 
						HBRUSH hbrush;                  // brush handle
						
						// Initialize CHOOSECOLOR 
						ZeroMemory(&cc, sizeof(cc));
						cc.lStructSize = sizeof(cc);
						cc.hwndOwner = hWnd;
						cc.lpCustColors = (LPDWORD) acrCustClr;
						cc.rgbResult = rgbCurrent;
						cc.Flags = CC_FULLOPEN | CC_RGBINIT;
 
						if (ChooseColor(&cc)==TRUE) 
						{
							 hbrush = CreateSolidBrush(cc.rgbResult);
							 rgbCurrent = cc.rgbResult; 
						}		
				}
				break;

				case IDM_ABOUT:
					{
						DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);

						  return 0;
					}
					break;
				case IDM_EXIT:
					{
						DestroyWindow(hWnd);
					}
					break;
				case IDM_View:
				{
					 CreateWindowEx(NULL, L"Class", NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL, 0, 0, 500, 80, hWnd, (HMENU) 1, hInst, NULL);
				}
//				default:
//					std::cout << "\n ::  " << LOWORD(wParam);
			}

			CheckShapeItem(hWnd,shape_mode);			
     }  
	break;

	{


		" C O N T R O L S   E N D "; 	


	}

	/*FOR CONTROLS END*/

		/*	
		case WM_TIMER:
		{
			hdc = GetDC(hWnd);
			 
				SetMode(hdc,cxClient,cyClient);

			KillTimer(hWnd,1);
			{
				shape * shp;
				shp = new RectangleShape(Cursor,PointRand(Cursor,loc),crRand(),crRand());
					lsp->PushBack(shp);
					shp->Print(hdc);
				delete shp;
			}
			ReleaseDC(hWnd,hdc);
		}
		break;
		*/

		
		case WM_VSCROLL :
			{
				hdc = GetDC(hWnd);

				SCROLLINFO siInfo;
				siInfo.cbSize = sizeof(SCROLLINFO);
				siInfo.fMask = SIF_POS|SIF_RANGE;
				GetScrollInfo(hWnd,SB_VERT,&siInfo);

				RECT rectTemp;
				GetClientRect(hWnd,&rectTemp);

				int dy = 0;
				int dPage = 0;

			   switch(LOWORD(wParam)){
				case SB_LINEUP :
				{
					dy = -1;
					if(siInfo.nPos == 0) dy = 0;
					dPage = dy;
				}
				break;
				case SB_LINEDOWN :
					{
					dy = 1;
					if(siInfo.nPos == 1000) dy = 0;
					dPage = dy;
				}
				break;
				case SB_PAGEUP :
					{
					dy = -rectTemp.bottom;
					dPage = -200;
					if(siInfo.nPos + dPage <= 0)
					{
						dy = -siInfo.nPos;// - 1000;
					}
					dPage = dy;
				}
				break;
				case SB_PAGEDOWN :
					{
					dy = rectTemp.bottom;
					dPage = 200;
					if(siInfo.nPos + dPage >= siInfo.nMax)
					{
						dy = siInfo.nMax - siInfo.nPos;
					}
					dPage = dy;
				}
				break;
				case SB_THUMBTRACK:
				{
					dy = HIWORD(wParam) - abs(iVscrollPos);
					dPage = dy;
 				}
				break;
				default :
					break;
			}

				iVscrollPos -= dy;
				ScrollWindow(hWnd,0,-dy,0,0);

				siInfo.cbSize =  sizeof(SCROLLINFO);
				siInfo.fMask = SIF_POS;
				siInfo.nPos += dPage;

				SetScrollInfo(hWnd,SB_VERT,&siInfo,TRUE);

			ReleaseDC(hWnd,hdc);
		}
		break;

		case WM_HSCROLL :
			{

				hdc = GetDC(hWnd);

				SCROLLINFO siInfo;
				siInfo.cbSize = sizeof(SCROLLINFO);
				siInfo.fMask = SIF_POS|SIF_RANGE;
				GetScrollInfo(hWnd,SB_HORZ,&siInfo);

				RECT rectTemp;
				GetClientRect(hWnd,&rectTemp);

				int dx = 0;
				
				switch(LOWORD(wParam)){
					case SB_LINELEFT :
						{
							dx = -1;
							if(siInfo.nPos == 0) dx = 0;
						}
						break;
					case SB_LINERIGHT :
						{
							dx = 1;
							if(siInfo.nPos == siInfo.nMax) dx = 0;
						}
							break;
					case SB_PAGELEFT :
						{
							dx = -rectTemp.bottom;
							if(siInfo.nPos + dx <= 0)
							{
								dx = -siInfo.nPos;
							}
						}
						break;
					case SB_PAGERIGHT :
						{
							dx = rectTemp.bottom;
							if(siInfo.nPos + dx >= siInfo.nMax)
							{
								dx = siInfo.nMax - siInfo.nPos;
							}
						}
						break;
				 case SB_THUMBTRACK:
					 {
							dx = HIWORD(wParam) - abs(iHscrollPos);
					 }
						break;
					default :
					break;
				}

				iHscrollPos -= dx;
				ScrollWindow(hWnd,-dx,0,0,0);

					siInfo.cbSize =  sizeof(SCROLLINFO);
					siInfo.fMask = SIF_POS;
					siInfo.nPos += dx;
						
					SetScrollInfo(hWnd,SB_HORZ,&siInfo,TRUE);

				ReleaseDC(hWnd,hdc);
			}
		break;

		case WM_LBUTTONDOWN:
			cout << "\nWM_LBUTTONDOWN";
			{ 
			hdc = GetDC(hWnd);

				SetMode(hdc,cxClient,cyClient,iHscrollPos,iVscrollPos);

				GetCursorPos(&Cursor);
				ScreenToClient(hWnd,&Cursor);
					DrawBoxOutline(hWnd, Cursor, Cursor);
				//DPtoLP(hdc,&Cursor,1);
				
				ptBeg = Cursor;
				DrawBoxOutline(hWnd, ptBeg, Cursor);
				fBlocking = TRUE;

			ReleaseDC(hWnd,hdc);
		}
		break;

		case WM_MOUSEMOVE:
			if(fBlocking)
					{
						SetCapture(hWnd);

					//	hdc = GetDC(hWnd);

								DrawBoxOutline(hWnd, ptBeg, Cursor);
						
								GetCursorPos(&Cursor);
								ScreenToClient(hWnd,&Cursor); 

								DrawBoxOutline(hWnd, ptBeg, Cursor);

					//	ReleaseDC(hWnd,hdc);
					}
		break;

		case WM_LBUTTONUP:
			if(fBlocking)
			{
				hdc = GetDC(hWnd);
					SetMode(hdc,cxClient,cyClient,iHscrollPos,iVscrollPos);
							
						GetCursorPos(&Cursor);
						ScreenToClient(hWnd,&Cursor);

						DPtoLP(hdc,&Cursor,1);
						DPtoLP(hdc,&ptBeg,1);

						shape * shp = new RectangleShape(ptBeg,Cursor,/*crRand()*/rgbCurrent,/*crRand()*/rgbCurrent);
				 			lsp->PushBack(shp);
							shp->Print(hdcOffscreen);
						delete shp;

					fBlocking = FALSE;

					ReleaseCapture();
				ReleaseDC(hWnd,hdc);
				InvalidateRect(hWnd, NULL, TRUE);
			}
		break;

		case WM_KEYDOWN:
			{
			hdc = GetDC(hWnd);
			SetMode(hdc,cxClient,cyClient,iHscrollPos,iVscrollPos);

			shape * shp; 

			GetCursorPos(&Cursor);		
			ScreenToClient(hWnd,&Cursor);
			DPtoLP(hdc,&Cursor,1);
	
			switch(wParam)
			{ 
				case 'O':
						{
								shp = new CircleShape(Cursor,rand()%loc,crRand(),crRand());
								lsp->PushBack(shp);
								shp->Print(hdcOffscreen);
								
								delete shp;
						}
					break;
				case 'T':
						{
								shp = new TriangleShape(Cursor,PointRand(Cursor,loc),PointRand(Cursor,loc),crRand(),crRand());
								lsp->PushBack(shp);
								shp->Print(hdcOffscreen);
								delete shp;
						}
					break;
				case 'P':
						{								
								shp = new RectangleShape(Cursor,PointRand(Cursor,loc),crRand(),crRand());
								lsp->PushBack(shp);
								shp->Print(hdcOffscreen);
								delete shp;
						}
					break;

				case 'M':
						{
							switch(shape_mode)
							{
								case IDM_TRIANGLE:
									{
											shp = new TriangleShape(Cursor,PointRand(Cursor,loc),PointRand(Cursor,loc),crRand(),crRand());
											lsp->PushBack(shp);
											shp->Print(hdcOffscreen);
											delete shp;
									}
								break;
								case IDM_RECTANGLE:
									{								
											shp = new RectangleShape(Cursor,PointRand(Cursor,loc),crRand(),crRand());
											lsp->PushBack(shp);
											shp->Print(hdcOffscreen);
											delete shp;
									}
									break;
								case IDM_CIRCLE:
									{
											shp = new CircleShape(Cursor,rand()%loc,crRand(),crRand());
											lsp->PushBack(shp);
											shp->Print(hdcOffscreen);
											delete shp;
									}
									break;
							}
					
						}
						break;

				case 'D':
						{								
								lsp->Clear();
								UpdateWindow(hWnd);
						}
					break;
			}
			ReleaseDC(hWnd,hdc);
			
			
				RECT d;

				d.left = Cursor.x - loc;
				d.top = Cursor.y - loc;

				d.right = Cursor.x + loc;;
				d.bottom = Cursor.y + loc;
			InvalidateRect(hWnd, &d, FALSE);
		}
		break;

		case WM_LBUTTONDBLCLK:
			{
				hdc = GetDC(hWnd);
				SetMode(hdc,cxClient,cyClient,iHscrollPos,iVscrollPos);

				Cursor.x = LOWORD(lParam);
				Cursor.y = HIWORD(lParam);
				{
					shape * shp = new TriangleShape(Cursor,PointRand(Cursor,loc),PointRand(Cursor,loc),crRand(),crRand());
					lsp->PushBack(shp);
					shp->Print(hdcOffscreen);
					delete shp;
				}
									
				ReleaseDC(hWnd,hdc);
				RECT r;
				r.left = Cursor.x - loc;
				r.top  = Cursor.y - loc;
				r.right = Cursor.x + loc;
				r.bottom = Cursor.y + loc;

				InvalidateRect(hWnd, &r, TRUE);
		}
		break;

		case WM_RBUTTONDOWN:
			{
				hdc = GetDC(hWnd);
				SetMode(hdc,cxClient,cyClient,iHscrollPos,iVscrollPos);

				Cursor.x = LOWORD(lParam);
				Cursor.y = HIWORD(lParam);
				{
					shape * shp = new CircleShape(Cursor,rand()%loc,crRand(),crRand());
					lsp->PushBack(shp);
					shp->Print(hdcOffscreen);
					delete shp;
				}
			ReleaseDC(hWnd,hdc);
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;

		case WM_SIZE:
			{
				cxClient = LOWORD(lParam);
				cyClient = HIWORD(lParam);
				

				SendMessage(hStatusBar, WM_SIZE, 0, 0);

				/*
				SCROLLINFO siInfo;
				GetScrollInfo(hWnd,SB_HORZ,&siInfo);			
		*/
//						MoveWindow(hWnd,0,  0, 1280,900, TRUE);	
//					SCROLLINFO siInfo;
//					siInfo.cbSize =  sizeof(SCROLLINFO);
//					GetScrollInfo(hWnd,SB_VERT,&siInfo);
//						siInfo.fMask = SIF_PAGE;
////					siInfo.nPage = (cyClient/1000)*siInfo.nPage;

//						siInfo.nPage = HIWORD(lParam)/5;
//						SetScrollInfo(hWnd,SB_VERT,&siInfo,TRUE);

//					GetScrollInfo(hWnd,SB_HORZ,&siInfo);
//					SIF_PAGE;
//						SetScrollInfo(hWnd,SB_HORZ,&siInfo,TRUE);
			}
		break;

		case WM_SETCURSOR:
			{
			
					switch (LOWORD(lParam))
					{
//						case HTCLIENT:
//						case HTCAPTION: 
//						case HTSYSMENU:  
//						case HTMENU:
//						case HTHSCROLL:
//						case HTVSCROLL:
//						case HTMINBUTTON:
//						case HTMAXBUTTON:  					
//						case HTCLOSE: 
//							{							
//								SetCursor(LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR1)));
//							}
//							break;
						case HTLEFT: 
						case HTRIGHT: 
							{
								SetCursor(LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR3)));
							}
							break;
						case HTTOP:
						case HTBOTTOM:
							{
								SetCursor(LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR4)));
							}
							break;
						case HTTOPRIGHT:
						case HTBOTTOMLEFT:
							{
								SetCursor(LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR5)));
							}
							break;
						case HTTOPLEFT:
						case HTBOTTOMRIGHT:
							{
								SetCursor(LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR6)));
							}
							break;
						default:
							SetCursor(LoadCursor(hInst, MAKEINTRESOURCE(IDC_CURSOR1)));
					}
											
			}
		break;

		case WM_DESTROY:
			{
				PostQuitMessage(0);
				delete lsp;

				DeleteObject(hBitmap);
				DeleteObject(hfont);
				
//				temp.close();
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
			{
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}

// ���������� ��������� ��� ���� "������".
INT_PTR CALLBACK Dialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
			{
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}