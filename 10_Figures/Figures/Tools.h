#pragma once
class Tools
{
public:
  Tools(void);
  ~Tools(void);

  static COLORREF colorRand();
  static POINT pointRand(POINT pDOT,int Radius = 0,HWND hWn = NULL);
  static void fillListRand(ListShape* &lsp,size_t numberOfItems,int loc,HWND hWnd);
  static void pushRandomTriangle(ListShape* &lsp, const POINT &point,size_t radius, HWND hwnd);
};

