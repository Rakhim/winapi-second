#include "StdAfx.h"
#include "Tools.h"


Tools::Tools(void) {
}

Tools::~Tools(void) {
}

void Tools::pushRandomTriangle(ListShape* &lsp, const POINT &point,size_t radius, HWND hwnd) {
  COLORREF cBurder  = colorRand();
  COLORREF cInterface = colorRand();
  Shape *shp = new TriangleShape(point, pointRand(point, radius), pointRand(point, radius), cBurder, cInterface);
	lsp->Push(shp);
	delete shp;
}

void Tools::fillListRand(ListShape* &lsp,size_t numberOfItems,int loc,HWND hWnd)
{
	lsp = new ListShape();
	Shape * shp;

	for(size_t i = 0;i < numberOfItems;i++)
	{
		POINT pDOT = {0,0};
		pDOT = pointRand(pDOT,0,hWnd);
		COLORREF cBurder  = colorRand();
		COLORREF cInterface = colorRand();

		switch(rand()%3)
		{
			case 0:
			{
				shp = new CircleShape(pDOT, rand() % (loc / 3), cBurder, cInterface);
				lsp->Push(shp);
				delete shp;
			}
			break;
			case 1:
			{
				shp = new TriangleShape(pDOT, pointRand(pDOT, loc), pointRand(pDOT, loc), cBurder, cInterface);
				lsp->Push(shp);
				delete shp;
			}
			break;
			case 2:
			{
				shp = new RectangleShape(pDOT, pointRand(pDOT, loc), cBurder, cInterface);
				lsp->Push(shp);
				delete shp;
			}
			break;
		}
	}
}



COLORREF Tools::colorRand()
{
	 return RGB(rand()%256,rand()%256,rand()%256);
}

POINT Tools::pointRand(POINT pDOT, int Radius, HWND hWn)
{
	POINT res = pDOT;

	if((Radius > 0) && !hWn)
	{
		res.x += rand() % (2 * Radius) - Radius;
		res.y += rand() % (2 * Radius) - Radius;
	} else
	if(hWn != NULL)
	{
		RECT rectTemp;
		GetClientRect(hWn,&rectTemp);

		res.x = rand() % rectTemp.right;
		res.y = rand() % rectTemp.bottom;
	}
	return res;
}

