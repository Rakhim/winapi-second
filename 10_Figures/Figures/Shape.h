#pragma once
#include "Printable.h"
class Shape:
  public Printable
{
public:
	Shape(void);

	virtual Shape* Clone() = 0;
	virtual void print(HDC &hdc) = 0;
	virtual ~Shape();
};

