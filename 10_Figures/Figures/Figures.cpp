// Figures.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Figures.h"

#define MAX_LOADSTRING 100

// ���������� ����������:
// thread_local ~ (__declspec(thread)) for separate global and static variables in threads
__declspec(thread) HINSTANCE hInst;
TCHAR szTitle[MAX_LOADSTRING];
TCHAR szWindowClass[MAX_LOADSTRING];
TCHAR szChildWindowClass[] = TEXT("child_window");
const size_t radius = 70;
// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM				MyRegisterClass(HINSTANCE hInstance);
ATOM        MyRegisterChildClass(HINSTANCE hInstance);

BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

#define MAX_THREADS 3

typedef struct MyData {
  HINSTANCE hInstance;
  int       nCmdShow;
} MYDATA, *PMYDATA;

DWORD WINAPI winThread(LPVOID lpParam);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
 
  LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_FIGURES, szWindowClass, MAX_LOADSTRING);

	MyRegisterClass(hInstance);
  MyRegisterChildClass(hInstance);

  HANDLE  hThreadArray[MAX_THREADS];
  PMYDATA pDataArray[MAX_THREADS];

  for(size_t i = 0; i < MAX_THREADS; i++) {
    pDataArray[i] = new MYDATA;
    pDataArray[i]->hInstance = hInstance;
    pDataArray[i]->nCmdShow = nCmdShow;
  }
  
  for(size_t i = 0; i < MAX_THREADS; i++) {
    hThreadArray[i] = CreateThread( 
        NULL,                   // default security attributes
        0,                      // use default stack size  
        winThread,              // thread function name
        pDataArray[i],          // argument to thread function 
        0,                      // use default creation flags 
        NULL);                  // returns the thread identifier 
    if (NULL == hThreadArray[i])  {
      Error::ErrorExit(TEXT("CreateThread"));
      return 0;
    }
  }

  DWORD reValue = WaitForMultipleObjects(MAX_THREADS, hThreadArray, TRUE, INFINITE);
  for(size_t i = 0; i < MAX_THREADS; i++) {
    delete pDataArray[i];
  }
  return reValue;
}

DWORD WINAPI winThread(LPVOID lpParam) {
  PMYDATA pData = (PMYDATA)lpParam;
	MSG msg;
  
	if (!InitInstance(pData->hInstance, pData->nCmdShow)) {
		return FALSE;
	}

  HACCEL hAccelTable;
	hAccelTable = LoadAccelerators(pData->hInstance, MAKEINTRESOURCE(IDC_FIGURES));

	while (GetMessage(&msg, NULL, 0, 0)) 
  {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))	{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance) {
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FIGURES));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_FIGURES);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

ATOM MyRegisterChildClass(HINSTANCE hInstance) {
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= ChildWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FIGURES));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_FIGURES);
	wcex.lpszClassName	= szChildWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) 
{
  HWND hWnd;
   
  hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

  hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
    CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

  if (!hWnd) {
    return FALSE;
  }

  ShowWindow(hWnd, nCmdShow);
  UpdateWindow(hWnd);
   
   // child
  HWND childHwnd;
  RECT r;
	GetClientRect(hWnd,&r);
  childHwnd = CreateWindow(szChildWindowClass, szTitle, WS_CHILD,
      CW_USEDEFAULT, CW_USEDEFAULT, r.right, r.bottom, hWnd, NULL, hInstance, NULL);
   
  if (!childHwnd) {
    Error::ErrorExit(TEXT("CreateWindow"));
    return FALSE;
  }

  ShowWindow(childHwnd, nCmdShow);
  UpdateWindow(childHwnd);

  return TRUE;
}

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
  int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
  static __declspec(thread) ListShape *ls;
  static __declspec(thread) Offscreen *offscreen;
  static __declspec(thread) CHAR lbuttonDown;

  switch (message) {
  case WM_CREATE: {
    Tools::fillListRand(ls, 5, radius, hWnd);
    hdc= GetDC(hWnd);
    int cx = GetSystemMetrics(SM_CXSCREEN);
		int cy = GetSystemMetrics(SM_CYSCREEN);
    offscreen = new Offscreen(hdc, cx, cy);
    offscreen->write(ls);
    lbuttonDown = 0;
    ReleaseDC(hWnd, hdc);
    }
    break;
	case WM_PAINT: {
    hdc = BeginPaint(hWnd, &ps);
		// TODO: �������� ����� ��� ���������...
    RECT r;
		GetClientRect(hWnd,&r);
    offscreen->print(hdc, &r);
		EndPaint(hWnd, &ps);
    }
    break;
  case WM_LBUTTONDOWN: 
    lbuttonDown = 1;
  case WM_MOUSEMOVE:
    if (1 == lbuttonDown) {
      POINT point;
      point.x = LOWORD(lParam);
      point.y = HIWORD(lParam);
      COLORREF cBurder  = Tools::colorRand();
      COLORREF cInterface = Tools::colorRand();
      Shape *shp = new TriangleShape(point, Tools::pointRand(point, radius),
        Tools::pointRand(point, radius), cBurder, cInterface);
      offscreen->write(shp);
	    ls->Push(shp);
	    delete shp;
      RECT rect;
      rect.left = point.x - radius;
      rect.top = point.y - radius;
      rect.right = point.x + radius;
      rect.bottom = point.y + radius;
      InvalidateRect(hWnd, &rect, FALSE);
    }    
    break;
  case WM_LBUTTONUP:
    if (1 == lbuttonDown) {
      lbuttonDown = 127;
    }
    break;
  case WM_DESTROY:
    delete ls;
    delete offscreen;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
  return NULL;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT: {
		hdc = BeginPaint(hWnd, &ps);
		// TODO: �������� ����� ��� ���������...
    
		EndPaint(hWnd, &ps);
    }
		break;
  case WM_MOUSEMOVE: {
    hdc= GetDC(hWnd);
    POINT point;
    GetCursorPos(&point);
    ScreenToClient(hWnd,&point);
		DPtoLP(hdc, &point, 1);
    COLORREF cBorder  = Tools::colorRand();
    COLORREF cInterface = Tools::colorRand();
    Shape *shp = new RectangleShape(point,
    Tools::pointRand(point, radius), cBorder, cInterface);
    shp->Print(hdc);
    ReleaseDC(hWnd, hdc);
    }
    break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}