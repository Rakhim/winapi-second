#pragma once
class Printable
{
public:
  virtual void print(HDC &hdc) = 0;
  Printable(void);
  virtual ~Printable(void);
};

