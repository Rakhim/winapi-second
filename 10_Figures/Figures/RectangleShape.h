#pragma once
#include "shape.h"
class RectangleShape :
	public Shape
{
private:
	COLORREF cBurder;
	COLORREF cInterface;
	int ileft,itop,iright,ibottom;
public:
	RectangleShape(void): cBurder(0x000000),cInterface(0x000000),ileft(0),itop(0),iright(0),ibottom(0){};
	RectangleShape(const RectangleShape& p)
	{
		cBurder = p.cBurder;
		cInterface = p.cInterface;

		ileft = p.ileft;
		itop = p.itop;
		iright = p.iright;
		ibottom = p.ibottom;
	}
		
	RectangleShape(POINT leftTop,POINT rightBottom,COLORREF crBurder): cBurder(crBurder),cInterface(crBurder),ileft(leftTop.x),itop(leftTop.y),iright(rightBottom.x),ibottom(rightBottom.y){}
	RectangleShape(POINT leftTop,POINT rightBottom,COLORREF crBurder,COLORREF crInterface): cBurder(crBurder),cInterface(crInterface),ileft(leftTop.x),itop(leftTop.y),iright(rightBottom.x),ibottom(rightBottom.y){}

	virtual Shape* Clone()
	{
		return new RectangleShape(*this);
	}

	virtual void print(HDC& hdc)
	{
		HPEN hpen,hpenOld;
		HBRUSH hbrush,hbrushOld;
				
		 hpen = CreatePen(PS_SOLID, 1, cBurder);
		 hbrush = CreateSolidBrush(cInterface);

		 hpenOld = (HPEN)SelectObject(hdc,hpen);
		 hbrushOld = (HBRUSH)SelectObject(hdc,hbrush);

			  Rectangle(hdc,ileft,itop,iright,ibottom);

		   SelectObject(hdc,hpenOld);
			SelectObject(hdc,hbrushOld);

		 DeleteObject(hbrush);
		 DeleteObject(hpen);
		
	}

	~RectangleShape(void){};
};