#include "MappedFile.h"


MappedFile::MappedFile(LPCSTR lpFileName, DWORD dwMaximumSize):
  hFile(INVALID_HANDLE_VALUE),
  hMap(INVALID_HANDLE_VALUE),
  lpBaseAddress(NULL),
  countOfMaps(0)
{
  this->dwMaximumSize = dwMaximumSize;

  hFile = CreateFile(lpFileName,
    GENERIC_READ | GENERIC_WRITE,
    NULL,
    NULL,
    OPEN_ALWAYS,
    FILE_ATTRIBUTE_NORMAL,
    NULL);

  if (INVALID_HANDLE_VALUE == hFile) {
    MAPPED_FILE_EXCEPTION ex;
    ex.message = "Constructor : file";
    throw ex;
  }

  hMap = CreateFileMapping(hFile,
  NULL,
  PAGE_READWRITE,
  0,
  dwMaximumSize,
  NULL);
  if (INVALID_HANDLE_VALUE == hFile) {
    CloseHandle(hFile);
    MAPPED_FILE_EXCEPTION ex;
    ex.message = "FileMapping : file";
    throw ex;
  }
}

VOID MappedFile::map(DWORD dwNumberOfBytesToMap) {
  LPVOID pView = MapViewOfFile(hMap,
  FILE_MAP_ALL_ACCESS,
  0,
  0,
  dwNumberOfBytesToMap);
  
  if (NULL == pView) {
    MAPPED_FILE_EXCEPTION e;
    e.message = "MappedFile.commit : MapViewOfFile";
    throw e;
  }
  lpBaseAddress = pView;  
}

VOID MappedFile::unmap() {
  UnmapViewOfFile(lpBaseAddress);
}

MappedFile::~MappedFile(VOID) {
  unmap();
  CloseHandle(hMap);
  CloseHandle(hFile);
}

DWORD MappedFile::getDword(DWORD index) {
  
  size_t newCountOfMaps = bytesToMaps((index + 1) * sizeof(DWORD));
  if (newCountOfMaps > countOfMaps) {
    unmap();
    map(newCountOfMaps * MAP_SIZE);
    countOfMaps = newCountOfMaps;
  }
  
  return (DWORD)((DWORD*)lpBaseAddress)[index];
}

BOOL MappedFile::setDword(DWORD index, DWORD value) {
  size_t newCountOfMaps = bytesToMaps((index + 1) * sizeof(DWORD));
  if (newCountOfMaps > dwMaximumSize / MAP_SIZE) return FALSE;
  if (newCountOfMaps > countOfMaps) {
    unmap();
    
    map(newCountOfMaps * MAP_SIZE);
    countOfMaps = newCountOfMaps;
  }
  
  (DWORD)((DWORD*)lpBaseAddress)[index] = value;
  return TRUE;
}

DWORD& MappedFile::operator[] (DWORD index) {

  size_t newCountOfMaps = bytesToMaps((index + 1) * sizeof(DWORD));
  if (newCountOfMaps > dwMaximumSize / MAP_SIZE) { 
    throw;
  }
  if (newCountOfMaps > countOfMaps) {
    unmap();
    map(newCountOfMaps * MAP_SIZE);
    countOfMaps = newCountOfMaps;
  }
  return (DWORD)((DWORD*)lpBaseAddress)[index];
}

LPCWSTR MappedFile::getString(DWORD offsetInWchars, DWORD length) {
  if (0 > offsetInWchars || 1 > length) return NULL;
  
  DWORD newCountOfMaps = bytesToMaps((offsetInWchars + length) * sizeof(WCHAR));
  if (newCountOfMaps > dwMaximumSize / MAP_SIZE) return NULL;

  if (newCountOfMaps > countOfMaps) {
    unmap();
    map(newCountOfMaps * MAP_SIZE);
    countOfMaps = newCountOfMaps;
  }
  return (LPCWSTR)lpBaseAddress + offsetInWchars;
}

BOOL MappedFile::setString(DWORD offsetInWchars, LPCWSTR value, DWORD length) {
  if (0 > offsetInWchars || 1 > length) return NULL;
  
  DWORD newCountOfMaps = bytesToMaps((offsetInWchars + length) * sizeof(WCHAR));
  if (newCountOfMaps > dwMaximumSize / MAP_SIZE) return NULL;

  if (newCountOfMaps > countOfMaps) {
    unmap();
    map(newCountOfMaps * MAP_SIZE);
    countOfMaps = newCountOfMaps;
  }
  CopyMemory((LPVOID)((LPWSTR)lpBaseAddress + offsetInWchars), value, length * sizeof(WCHAR));
}

size_t MappedFile::bytesToMaps(size_t bytes) {
  size_t size = bytes / MAP_SIZE;
  return (bytes % MAP_SIZE ? size + 1 : size);
}