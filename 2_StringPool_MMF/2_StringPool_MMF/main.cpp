#include "StringPool.h"
#include "GetLastError.h"

DWORD FilterFunction();

//#include <Windows.h>
//#include <stdio.h>
//#include <tchar.h>
//#include <strsafe.h>
#include <iostream>

void check() {
  try {
    StringPool sp("out", "outIndexes");
    size_t i = 0;
    // 2^25(33554432) strings with size 3 time: 1 minute 04 sec
    //       44739243 strings with size 2 time: 1 minute 29 sec
    // 2^26(67108863) strings with size 1 time: 2 minute 22 sec 
    for (i = 0; i < (1 << 10) && sp.addString(L"%", 1); i++) {
      
    }

    for (size_t i = 0; i < (1 << 10); i++) {
      wprintf(L"%S\n", sp[i]);
      if (0 == i % 10) {
        system("pause");
      }
    }
    printf("\n size : %d \n stopped index : %d", sp.getCount(), i);
  } catch (MAPPED_FILE_EXCEPTION e) {
    ErrorExit(e.message);
  } catch (STRING_POOL_EXCEPTION e) {
    printf("\n%s", e.message);
  }
}

int main(int argc, char **argv) {
  __try {
    check();
  } __except(FilterFunction()) {	
    
	  switch(GetExceptionCode()) {
		  case EXCEPTION_ACCESS_VIOLATION:
				  printf("\nEXCEPTION_ACCESS_VIOLATION");
			  break;		
		  case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
				  printf("\nEXCEPTION_ARRAY_BOUNDS_EXCEEDED");
			  break;
		  case EXCEPTION_INT_DIVIDE_BY_ZERO:
				  printf("\nEXCEPTION_INT_DIVIDE_BY_ZERO");
			  break;
	  }
	}
  printf("\n");
  system("pause");
  return 0;
}

DWORD FilterFunction(){
  return EXCEPTION_EXECUTE_HANDLER; 
}