#include "StringPool.h"

StringPool::StringPool(LPCSTR lpFileName, LPCSTR lpFileNameOfIndexes):
  countOfStrings(0)
{
  mf = new MappedFile(lpFileName, STRING_POOL_MAX_SIZE);
  mfIndexes = new MappedFile(lpFileNameOfIndexes, STRING_POOL_MAX_SIZE / (2 * sizeof(WCHAR)) * sizeof(DWORD));
  countOfStrings = mfIndexes->getDword(0);
  mfIndexes->setDword(0, 0);
}

StringPool::~StringPool(void) {
  mfIndexes->setDword(0, countOfStrings);
  delete mf;
  delete mfIndexes;
}

BOOL StringPool::addString(const LPCWSTR lpString, DWORD length) {
  if (!lpString || !length) {
    return FALSE;
  }

  DWORD sizeInBytes = sizeof(WCHAR) * (length + 1);
  if (STRING_POOL_MAX_STRING_SIZE < sizeInBytes) {
    return FALSE;
  }
  
  if (mfIndexes->getDword(countOfStrings) + sizeInBytes > STRING_POOL_MAX_SIZE) {
    STRING_POOL_EXCEPTION e;
    e.message = "EOF";
    throw e;
  }

  if (mf->setString(mfIndexes->getDword(countOfStrings), lpString, length)) {
    if (FALSE == mfIndexes->setDword(countOfStrings + 1, mfIndexes->getDword(countOfStrings) + length + 1)) {
      return FALSE;
    }
  } else {
    return FALSE;
  }
  ++countOfStrings;
  return TRUE;
}

DWORD StringPool::getPageSize() {
  SYSTEM_INFO si;
  GetSystemInfo(&si);
  return si.dwPageSize;
}

LPCWSTR StringPool::operator[](DWORD index) {
  return (index < countOfStrings ?
 mf->getString(mfIndexes->getDword(index), STRING_POOL_MAX_STRING_SIZE) : NULL);
}

DWORD StringPool::getCount() {
  return countOfStrings;
}

DWORD StringPool::bytesToCountOfPages(DWORD bytes) {
  DWORD n = bytes / getPageSize();
  return (bytes % getPageSize() ? n + 1 : n);
}