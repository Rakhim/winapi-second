#include <windows.h>
#include "GetLastError.h"
#include <vector>
#include <list>

BOOL formattedInformation(const WIN32_FIND_DATA &findData, LPSTR lpFormattedFindData, SYSTEMTIME &systemTime) {
  if (FALSE == FileTimeToSystemTime(&findData.ftLastAccessTime, &systemTime)) {
    ErrorExit("FileTimeToSystemTime");
  }
  
  if (FALSE == SystemTimeToTzSpecificLocalTime(NULL, &systemTime, &systemTime)) {
    ErrorExit("SystemTimeToTzSpecificLocalTime");
  }

  return SUCCEEDED(StringCchPrintf(lpFormattedFindData, MAX_PATH, 
  ( FILE_ATTRIBUTE_DIRECTORY == findData.dwFileAttributes ?
 "%02u/%02u/%04u  %02u:%02u %2s%9s%9c %-20s \n" : "%02u/%02u/%04u  %02u:%02u %2s%9s%9u %-20s \n"),
  systemTime.wMonth,
  systemTime.wDay,
  systemTime.wYear,
  systemTime.wHour % 12,
  systemTime.wMinute,
  (systemTime.wHour < 12 ? "AM" : "PM"),
  (FILE_ATTRIBUTE_DIRECTORY == findData.dwFileAttributes ? "<DIR>" : " "),
  (FILE_ATTRIBUTE_DIRECTORY == findData.dwFileAttributes ? ' ' : findData.nFileSizeLow),
  findData.cFileName));
}

void list(LPCSTR lpcFileName) {
  
  WIN32_FIND_DATA findData;
  HANDLE hFirstFile;
  CHAR buffer[MAX_PATH];
  SYSTEMTIME systemTime;
  
  hFirstFile = FindFirstFile(lpcFileName, &findData);
  if (INVALID_HANDLE_VALUE == hFirstFile) {
    ErrorExit("FindFirstFileEx");
  }

  buffer[MAX_PATH];
  if (NULL == GetFullPathName(lpcFileName, MAX_PATH, buffer, NULL)) {
    ErrorExit("GetFullPathNameW");
  }
  
  size_t i = strlen(buffer) - 1;
  while ('\\' != buffer[i]) {
    --i;
  }
  buffer[i] = 0;
  if (FILE_ATTRIBUTE_DIRECTORY == findData.dwFileAttributes) {
    printf("\n %s%s\n\n", " ���������� ����� ", buffer);
  } else {
    
  }

  std::list<std::string> dirs;
  size_t numberOfFiles = 0;
  size_t summarySizeOfFiles = 0;
  size_t numberOfDirs = 0;
  do {
    CHAR formattedString[MAX_PATH];
    formattedInformation(findData, formattedString, systemTime);
    if (FILE_ATTRIBUTE_DIRECTORY == findData.dwFileAttributes) {
      if (findData.cFileName[0] == '.' || (findData.cFileName[0] == '.' && findData.cFileName[1] == '.')) {
      } else {
        std::string str;
        str += buffer;
        str += "\\";
        str += findData.cFileName;
        str += "\\*";
        dirs.push_back(str);
        ++numberOfDirs;
      }
    } else {
      ++numberOfFiles;
      summarySizeOfFiles += findData.nFileSizeLow;
    }
    printf("%s", formattedString);
  } while (FindNextFile(hFirstFile, &findData));
  printf("%17d %-6s%14d %-4s \n", numberOfFiles, "������", summarySizeOfFiles, "����");
  printf("%17d %-6s\n", numberOfDirs, " �����");
  FindClose(hFirstFile);
  
  for (std::list<std::string>::iterator i = dirs.begin(); i != dirs.end(); i++) {
    list(i->c_str());
  }
}

#include <locale.h>

int main(int argc, char **argv) {
  setlocale(0, "RUSSIAN");
  if (argc < 2) {
    list(".\\*");
  } else {
    list(argv[1]);
  }
  return 0;
}