#pragma once
#define MATRIX2DDLL_API __declspec(dllexport) 

#include <stdio.h>
#include <stdlib.h>

class MATRIX2DDLL_API Matrix2D {
public:
  static void zeroArray(int *value, size_t n);
  static void zeroMatrix(int **value, size_t nRows, size_t nColumns);
  static void createMatrix(int** &value, size_t nRows, size_t nColumns);
  static void deleteMatrix(int **value, size_t nRows);
  static void fillWithRandomValues(int **value, size_t nRows, size_t nColumns);
  static void showArray(int *value, size_t size);
  static void showMatrix(int **value, size_t nRows, size_t nColumns);
};

