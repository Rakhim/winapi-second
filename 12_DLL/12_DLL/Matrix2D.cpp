#include "Matrix2D.h"

void Matrix2D::zeroArray(int *value, size_t n) {
  for (size_t i = 0; i < n; i++) {
    value[i] = 0;
  }
}

void Matrix2D::zeroMatrix(int **value, size_t nRows, size_t nColumns) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      value[i][j] = 0;
    }
  }
}

void Matrix2D::fillWithRandomValues(int **value, size_t nRows, size_t nColumns) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      value[i][j] = rand() % 30 - 15;
    }
  }
}

void Matrix2D::createMatrix(int** &value, size_t nRows, size_t nColumns) {
  value = new int*[nRows];
  for (size_t i = 0; i < nRows; i++) {
    value[i] = new int[nColumns];
  }
}

void Matrix2D::showArray(int *value, size_t size) {
  printf("\n");
  for (size_t i = 0; i < size; i++) {
    printf("%d ", value[i]);
  }
}

void Matrix2D::showMatrix(int **value, size_t nRows, size_t nColumns) {
  for (size_t i = 0; i < nRows; i++) {
    for (size_t j = 0;j < nColumns; j++) {
      printf("%5d", value[i][j]);
    }
    printf("\n");
  }  
}

void Matrix2D::deleteMatrix(int **value, size_t nRows) {
  for (size_t i = 0; i < nRows; i++) {
    delete [] value[i];
  }
  delete [] value;
  value = NULL;
}