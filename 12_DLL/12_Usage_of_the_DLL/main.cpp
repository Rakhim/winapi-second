#include <windows.h> 
#include <stdio.h> 
#include "GetLastError.h"
#include "..\12_DLL\Matrix2D.h"

int main(int argc, char **argv) { 
  int **p;
  int nRows = 10;
  int nColumns = 10;
  Matrix2D::createMatrix(p, nRows, nColumns);
  Matrix2D::fillWithRandomValues(p, nRows, nColumns);
  Matrix2D::showMatrix(p, nRows, nColumns);
  Matrix2D::deleteMatrix(p, nRows);

  return 0;
}