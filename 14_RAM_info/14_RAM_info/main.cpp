#include <windows.h>
#include <tchar.h>
#include "GetLastError.h"
#include <LM.h>

#pragma comment(lib, "netapi32.lib")

#define DIV 1
#define WIDTH 7

void memoryInformation() {
  MEMORYSTATUSEX statex;
  statex.dwLength = sizeof (statex);
  ULONGLONG ullInstalledMemory;

  if (FALSE == GetPhysicallyInstalledSystemMemory(&ullInstalledMemory)) {
    ErrorExit("GetPhysicallyInstalledSystemMemory");
  }

  if (FALSE == GlobalMemoryStatusEx(&statex)) {
    ErrorExit("GetPhysicallyInstalledSystemMemory");
  }

  _tprintf("\n%20s\n\n", "Memory");
  _tprintf("%-18s %14d%c\n", "Memory load", statex.dwMemoryLoad, '%');
  _tprintf("%-18s %15llu\n", "Installed memory",ullInstalledMemory << 10);
  _tprintf("%-18s %15d\n", "Total memory", statex.ullTotalPhys);
  _tprintf("%-18s %15d\n", "Free memory", statex.ullAvailPhys);
  _tprintf("%-18s %15d\n", "Total virtual", statex.ullTotalVirtual);
  _tprintf("%-18s %15d\n", "Free virtual", statex.ullAvailVirtual);
  _tprintf("%-18s %15d\n", "Total paging file", statex.ullTotalPageFile);
  _tprintf("%-18s %15d\n", "Free paging file", statex.ullAvailPageFile);
}

void volumeInformation(LPCSTR rootPathName) {
  CHAR volumeNameBuffer[MAX_PATH];
  DWORD volumeSerialNumber;
  CHAR fileSystemNameBuffer[MAX_PATH];
  if ( FALSE == GetVolumeInformation(rootPathName, volumeNameBuffer, MAX_PATH, &volumeSerialNumber, NULL, NULL, fileSystemNameBuffer, MAX_PATH)) {
    ErrorExit("GetVolumeInformation");
  }
  ULARGE_INTEGER freeBytesAvailable;
  ULARGE_INTEGER totalNumberOfBytes;
  ULARGE_INTEGER totalNumberOfFreeBytes;

  if (FALSE == GetDiskFreeSpaceEx(rootPathName, &freeBytesAvailable, &totalNumberOfBytes, &totalNumberOfFreeBytes)) {
    ErrorExit("GetDiskFreeSpaceEx");
  }
  
  UINT uDriveType = GetDriveType(rootPathName);
  char *sDriveType;
  switch (uDriveType) {
    case DRIVE_FIXED:
      sDriveType = "fixed";
      break;
    case DRIVE_CDROM:
      sDriveType = "CD-ROM";
      break;
    case DRIVE_RAMDISK:
      sDriveType = "RAM-disk";
      break;
    case DRIVE_REMOTE:
      sDriveType = "remote";
      break;
    case DRIVE_REMOVABLE:
      sDriveType = "removable";
      break;
    case DRIVE_NO_ROOT_DIR:
      sDriveType = "(invalid root path name)";  
      break;
    default:
      sDriveType = "(unknown drive type)";
  }
  CHAR volumePathName[MAX_PATH];
  if (FALSE == GetVolumePathName(rootPathName, volumePathName, MAX_PATH)) {
    ErrorExit("GetVolumePathName");
  }

  _tprintf("\n%20s\n\n", "Volume");
  _tprintf("%-18s %15s\n", "Drive letter", volumePathName);
  _tprintf("%-18s %15s\n", "Drive type", sDriveType);
  _tprintf("%-18s %15llu\n", "Total bytes", totalNumberOfBytes.QuadPart);
  _tprintf("%-18s %15llu\n", "Free bytes", freeBytesAvailable.QuadPart);
  _tprintf("%-18s %15s\n", "File system name", fileSystemNameBuffer);
  _tprintf("%-18s %15s\n", "Volume label", volumeNameBuffer);
  _tprintf("%-18s %15d\n", "Serial number", volumeSerialNumber);
}

void userInformation() {
  WCHAR computerName[MAX_PATH];
  DWORD nSize;
  
  if (FALSE == GetComputerNameExW(ComputerNameNetBIOS, computerName, &nSize)) {
    ErrorExit("GetComputerName");
  }
  WCHAR userName[MAX_PATH];
  if (FALSE == GetUserNameW(userName, &nSize)) {
    ErrorExit("GetUserName");
  }
  LPLOCALGROUP_INFO_0 lpLocalGroupInfo;
  DWORD entriesRead;
  DWORD totalEntries;
  
  NET_API_STATUS netApiStatus = NetUserGetLocalGroups(NULL,
   userName,
   0,
   LG_INCLUDE_INDIRECT,
   (LPBYTE*)&lpLocalGroupInfo,
   MAX_PREFERRED_LENGTH,
   &entriesRead,
   &totalEntries);

  _tprintf("%-18s %15S\n", "Computer name", computerName);
  _tprintf("%-18s %15S\n", "User name", userName);
  _tprintf("%-18s ", "Access");

  for (size_t i = 0; i < entriesRead; i++) {
    _tprintf("%S ", lpLocalGroupInfo[i].lgrpi0_name);
  }

  NetApiBufferFree(lpLocalGroupInfo);
  
  _tprintf("\n");
}

int main (int argc, char **argv) {
  
  userInformation();
  volumeInformation("D:\\");
  memoryInformation();
  system("pause");
  return 0;
}