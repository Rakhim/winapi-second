#pragma once
#include "cfurn.h"
class BufCfurn :
	public Cfurn
{
public:

	virtual furn * Clone()
	{
		return new BufCfurn(*this);
	}

	BufCfurn(void): Cfurn() {}
	BufCfurn(const char * q): Cfurn(q) {}
	BufCfurn(BufCfurn& q)
	{
		ps = q.ps;
	}
	~BufCfurn(void){}
};

