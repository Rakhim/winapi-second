#include <iostream>
#include "furn.h"

typedef furn* T;

class furnList {
private:
	struct Node {
		Node * next;
		T x;
		Node(): next(0) ,x(0) {}
		Node(T p)
		{
			x = p->Clone();
			next = 0;
		}
	}* head,* tail;

private:
 	void addToHead(T q) {
		Node * s = new Node(q);
		s->next = 0;

		if(tail && head) {
			s->next = head;
			head = s;
		} else tail = head = s;
	}

	void addToTail(T& q) {
		if (q) {
			Node * s = new Node(q);
			s->next = 0;
			if (tail && head) {
				tail->next = s;
				tail = s;
			} else tail = head = s;
		}
	}

	void deleteFromHead(Node * & ph) {
		Node * p = ph;
		if (p) {
			ph = ph->next;
		}
		delete p->x;
		delete p;
	}

public:
	furnList(): head(0),tail(0){ }

	furnList(const furnList& p) {
		Node * ps = p.head;
		while(ps->next)	{
			addToTail(ps->x);
			ps = ps->next;
		}
	}

	void Push(T x) {
		addToHead(x);
	}

	void PushBack(T x) {
		addToTail(x);
	}

	void Clear() {
		while (head) {
			deleteFromHead(head);
    }
	}

	void show() {
		Node * p = head;
		while (p) {
			std::cout << " " << (p->x)->Name() << '\n';
			p = p->next;
		}
	}
	
	furnList& operator = (const furnList& q)
	{
		if (this != &q)	{
			while (head) {
				deleteFromHead(head);
			}

			Node * ps = q.head;

			while (ps) {
				addToTail(ps->x);
				ps = ps->next;
			}
		}
		return *this;
	}

	~furnList() {
		while (head) {
			deleteFromHead(head);
		}
		head = tail = 0;
	}
};
