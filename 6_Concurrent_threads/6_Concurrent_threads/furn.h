#pragma once
#include "string.h"

class furn {
protected:
	string ps;
public:
	furn() {
  	ps;
  }

	furn(const char* q) {
		ps = q;
	}

	furn(const furn& q) {
		ps = q.ps;
	}

	virtual furn* Clone() = 0;

	virtual string& Name() {
  	return ps;
  };

	virtual ~furn() { };
};
