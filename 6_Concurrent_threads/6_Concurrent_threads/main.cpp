#include "furnList.h"
#include "ArmSitSfurn.h"  // ������
#include "BedLaySfurn.h"  // �������
#include "BookCabCfurn.h" // ������� �����
#include "BufCfurn.h"     // ������
#include "ChairSitSfurn.h"// ������
#include "SofaLaySfurn.h" // ������
#include "WarCabCfurn.h"  // �������� �����
#include "GetLastError.h"
#include <vld.h>

#define MAX_THREADS 7

DWORD WINAPI threadFunction(LPVOID lpParam);

furnList *pList;
CRITICAL_SECTION criticalSection;

int main(int agrc, char **argv) {
  HANDLE  hThreadArray[MAX_THREADS];
  pList = new furnList();

  InitializeCriticalSection(&criticalSection);

  furn **pFurniture = new furn*[MAX_THREADS];

  pFurniture[0] =	new	ArmSitSfurn("1");
  pFurniture[1] =	new	BedLaySfurn("2");	
  pFurniture[2] =	new	BookCabCfurn("3");
  pFurniture[3] =	new	BufCfurn("4");
  pFurniture[4] =	new	ChairSitSfurn("5");
  pFurniture[5] =	new	SofaLaySfurn("6");
  pFurniture[6] =	new	WarCabCfurn("7");

  for(size_t i = 0; i < MAX_THREADS; i++) {
    hThreadArray[i] = CreateThread( 
        NULL,
        0,
        threadFunction,
        pFurniture[i],
        0,
        NULL);
    if (NULL == hThreadArray[i])  {
      ErrorExit("CreateThread");
      return 0;
    }
  }
  
  WaitForMultipleObjects(MAX_THREADS, hThreadArray, TRUE, INFINITE);
  DeleteCriticalSection(&criticalSection);

  for (size_t i = 0; i < MAX_THREADS; i++) {
    delete pFurniture[i];
  }
  delete pFurniture;

  pList->show();
  delete pList;

  system("pause");
  return 0;
}

DWORD WINAPI threadFunction(LPVOID lpParam) {
  EnterCriticalSection(&criticalSection);

  pList->Push((furn*)lpParam);
  pList->Push((furn*)lpParam);
  Sleep(4);
  pList->Push((furn*)lpParam);

  LeaveCriticalSection(&criticalSection);
  return 0;
}