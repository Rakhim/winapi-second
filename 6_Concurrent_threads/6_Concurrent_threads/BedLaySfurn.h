#pragma once
#include "laysfurn.h"
class BedLaySfurn :
	public LaySfurn
{
public:

	virtual furn * Clone()
	{
		return new BedLaySfurn(*this);
	}

	BedLaySfurn(void): LaySfurn() {}
	BedLaySfurn(const char * q): LaySfurn(q) {}

	BedLaySfurn(BedLaySfurn& q)
	{
		ps = q.ps;
	}
	~BedLaySfurn(void){}
};

