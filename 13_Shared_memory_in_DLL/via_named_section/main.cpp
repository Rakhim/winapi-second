/*
dumbpbin.exe result
 Summary
  1000 .data
  1000 .idata
  2000 .rdata
  1000 .reloc
  1000 .rsrc
  1000 .shared
  4000 .text
10000 .textbss
*/

#include <windows.h>
#define MAX_SIZE 100
#pragma data_seg (".shared")
  char shared_string[MAX_SIZE] = "hello world";
  DWORD shared_dword = 0;
#pragma data_seg()
// set R-read W-write S-shared attributes to just created section(segment)
#pragma comment(linker, "/section:.shared,RWS")

BOOL WINAPI DllMain(HINSTANCE hinstDLL,  DWORD fdwReason, LPVOID lpvReserved) {
  switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
      ++shared_dword;
  }
  return TRUE;
} 


#ifdef __cplusplus 
extern "C" {    
#endif

#define THISDLLL_API __declspec(dllexport)

THISDLLL_API int __cdecl SetSharedData(const char *string) {
  size_t i;
  size_t length = strlen(string);
  for (i = 0; i < length && i < MAX_SIZE; i++) {
    shared_string[i] = string[i];
  }
  return i;
}

THISDLLL_API int __cdecl GetSharedData(char *string, int length) {
  size_t i;
  for (i = 0; i < length && i < MAX_SIZE; i++) {
    string[i] = shared_string[i];
  }
  return i;
}

THISDLLL_API DWORD __cdecl getNumberOfUsageSegment () {
  return shared_dword;
}

#ifdef __cplusplus
}
#endif