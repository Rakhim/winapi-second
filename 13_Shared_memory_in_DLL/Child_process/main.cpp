#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "GetLastError.h"

extern "C" VOID __cdecl GetSharedMem(LPWSTR lpszBuf, DWORD cchSize);
extern "C" int __cdecl GetSharedData(char *string, int length);

int main(int argc, char **argv) {
  const int size = 100;
  WCHAR cBuf[size];
  GetSharedMem(cBuf, size);

  char cBuffer[size];
  GetSharedData(cBuffer, size);
  printf("Child process read from shared memory:\n%S\n\n", cBuf);
  printf("Child process read from shared segment:\n%s\n\n", cBuffer);
  return 0;
}