#include <windows.h> 
#include <memory.h> 
#include "GetLastError.h"

#define SHARED_MEM_SIZE (1 << 12)
#define NAME_OF_MAP_OBJECT "dllmemfilemap"

static LPVOID lpvMem = NULL;      // pointer to shared memory
static HANDLE hMapObject = NULL;  // handle to file mapping

BOOL WINAPI DllMain(HINSTANCE hinstDLL,
  DWORD fdwReason,
  LPVOID lpvReserved) 
{
  BOOL fInit, fIgnore; 

  switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
      hMapObject = CreateFileMapping( 
          INVALID_HANDLE_VALUE,   // use paging file
          NULL,                   // default security attributes
          PAGE_READWRITE,         // read/write access
          0,                      // size: high 32-bits
          SHARED_MEM_SIZE,        // size: low 32-bits
          TEXT(NAME_OF_MAP_OBJECT)); // name of map object
        if (NULL == hMapObject) {
          ErrorExit(L"CreateFileMapping");
          return FALSE;
        }
 
        fInit = (GetLastError() != ERROR_ALREADY_EXISTS); 

        lpvMem = MapViewOfFile( 
          hMapObject,
          FILE_MAP_WRITE,
          0,
          0,
          SHARED_MEM_SIZE);
        if (NULL == lpvMem) { 
          ErrorExit(L"CreateFileMapping");
          return FALSE; 
        }
 
        if (fInit) { 
          memset(lpvMem, '\0', SHARED_MEM_SIZE); 
        }
        ++((LPDWORD)lpvMem)[1 << 10 - sizeof(DWORD)];
        break; 
      case DLL_THREAD_ATTACH: 
          break; 
 
      case DLL_THREAD_DETACH: 
          break; 
 
      case DLL_PROCESS_DETACH: 
          fIgnore = UnmapViewOfFile(lpvMem);  
          fIgnore = CloseHandle(hMapObject); 
          break; 
 
      default: 
        break; 
    } 
 
  return TRUE; 
  UNREFERENCED_PARAMETER(hinstDLL); 
  UNREFERENCED_PARAMETER(lpvReserved); 
} 

#ifdef __cplusplus
extern "C" {
#endif
 
#define THISDLLL_API __declspec(dllexport)

THISDLLL_API VOID __cdecl SetSharedMem(LPWSTR lpszBuf)  {
  LPWSTR lpszTmp; 
  DWORD dwCount=1;
  lpszTmp = (LPWSTR) lpvMem; 
  
  while (*lpszBuf && dwCount<SHARED_MEM_SIZE) {
      *lpszTmp++ = *lpszBuf++; 
      dwCount++;
  }
  *lpszTmp = '\0';
}

THISDLLL_API VOID __cdecl GetSharedMem(LPWSTR lpszBuf, DWORD cchSize) {
  LPWSTR lpszTmp; 
  lpszTmp = (LPWSTR) lpvMem;  
  while (*lpszTmp && --cchSize) { 
    *lpszBuf++ = *lpszTmp++; 
  }
  *lpszBuf = '\0';
}

THISDLLL_API DWORD __cdecl getNumberOfUsage () {
  return ((LPDWORD)lpvMem)[1 << 10 - sizeof(DWORD)];
}


#ifdef __cplusplus
}
#endif