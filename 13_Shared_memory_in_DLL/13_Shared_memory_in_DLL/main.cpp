// Parent process

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "GetLastError.h"

extern "C" VOID __cdecl SetSharedMem(LPWSTR lpszBuf);
extern "C" int __cdecl SetSharedData(const char *string);

extern "C" DWORD __cdecl getNumberOfUsage ();
extern "C" DWORD __cdecl getNumberOfUsageSegment ();

HANDLE CreateChildProcess(LPTSTR szCmdline) { 
  PROCESS_INFORMATION piProcInfo; 
  STARTUPINFO siStartInfo;
  BOOL bFuncRetn = FALSE; 
 
  ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) );
 
  ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) );
  siStartInfo.cb = sizeof(STARTUPINFO); 
    
  bFuncRetn = CreateProcess(NULL, 
  szCmdline,     // command line 
  NULL,          // process security attributes 
  NULL,          // primary thread security attributes 
  TRUE,          // handles are inherited 
  0,             // creation flags 
  NULL,          // use parent's environment 
  NULL,          // use parent's current directory 
  &siStartInfo,  // STARTUPINFO pointer 
  &piProcInfo);  // receives PROCESS_INFORMATION 
   
  if (NULL == bFuncRetn) {
    ErrorExit("CreateProcess");
  }
  else {
    CloseHandle(piProcInfo.hThread);
    return piProcInfo.hProcess;
  }
}

int main(int argc, char **argv) {
  printf("\nProcess is writing to shared memory...\n\n");

  SetSharedMem(L"That string was written into MMF in a DLL by my daddy. :) ");
  SetSharedData("And also this string via shared segment(section) ");

  HANDLE hProcess = CreateChildProcess("..\\Debug\\Child_process.exe");
  
  printf("Usage segment:: %d\n", getNumberOfUsageSegment());
  printf("    Usage mmf:: %d\n", getNumberOfUsage());

  WaitForSingleObject(hProcess, INFINITE);
  CloseHandle(hProcess);
  
  system("pause");
  return 0;
}
