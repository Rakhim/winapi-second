#include <windows.h>
#include "GetLastError.h"

BOOL run(LPCTSTR lpApplicationName,LPSTR lpCommandLine, PROCESS_INFORMATION &processInformation) {
  STARTUPINFO startUpInfo;

  ZeroMemory(&processInformation, sizeof(processInformation));
  ZeroMemory(&startUpInfo, sizeof(startUpInfo));
  startUpInfo.cb = sizeof(startUpInfo);

  return CreateProcess(lpApplicationName,
    lpCommandLine,
    NULL,
    NULL,
    FALSE,
    NORMAL_PRIORITY_CLASS,
    NULL,
    NULL,
    &startUpInfo,
    &processInformation
);
}


int main(int argc ,char **argv) {
  PROCESS_INFORMATION processInformation;
  run(TEXT("C:\\Windows\\notepad.exe"), TEXT(""), processInformation);
  
  WaitForSingleObject(processInformation.hProcess, INFINITE );

  CloseHandle(processInformation.hProcess );
  CloseHandle(processInformation.hThread );
  
  
  return 0;
}
