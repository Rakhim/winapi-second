#pragma once
#include <Windows.h>

struct REG_EXCEPTION {
  LSTATUS status;
  char *message;
};

class Reg {
public:
  Reg(LPCSTR);
  ~Reg(void);

  LSTATUS createSubKey(LPCSTR);
  LSTATUS deleteSubKey(LPCSTR);

  LPSTR enumSubKeys (DWORD);
  DWORD getCountOfSubKeys();
private:
  HKEY hKey;
  HKEY location;
  LPCSTR lpcKeyName;
  LSTATUS status;
  REG_EXCEPTION exception;
};