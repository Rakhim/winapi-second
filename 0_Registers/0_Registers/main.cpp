#include <windows.h>
#include <stdio.h>
#include "Reg.h"

int main(int argc, char** argv) {
  
  try {
    Reg reg("SUBK2");

    reg.createSubKey("Bone");
    reg.createSubKey("Btwo");
    reg.createSubKey("Ethree");
    reg.createSubKey("Bfour");

    reg.createSubKey("AAtive");
    reg.deleteSubKey("AAtive");

    reg.createSubKey("Ethree");

    for (DWORD i = 0;i < reg.getCountOfSubKeys();i++) {
      printf("\n%s", reg.enumSubKeys(i));
    }

    printf("\n\n");
  } catch (REG_EXCEPTION e) {
    printf("\n%s", e.message);
  } catch (...) {
    printf("\nsomething is happened\n");
  }

  system("pause");
	return 0;
}