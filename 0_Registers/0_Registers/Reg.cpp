#include "Reg.h"

Reg::Reg(LPCSTR lpcKeyName) {
  this->lpcKeyName = lpcKeyName;
  location = HKEY_CURRENT_USER;
  status = RegCreateKeyEx(location, this->lpcKeyName, NULL, NULL,
    REG_OPTION_VOLATILE | KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE ,
    KEY_ALL_ACCESS,
    NULL, 
		&hKey, NULL);
  if (ERROR_SUCCESS != status) {
    exception.status = status;
    exception.message = "constructor";
    throw exception;
  }
}

Reg::~Reg(void) {
  
  status = RegCloseKey(hKey);  
  if(ERROR_SUCCESS != status) {
    exception.status = status;
    exception.message = "Close handle";
    throw exception;
  }

  status = RegDeleteTree(location, this->lpcKeyName);
  if(ERROR_SUCCESS != status) {
    exception.status = status;
    exception.message = "RegDeleteTree";
    throw exception;
  }
}

LSTATUS Reg::createSubKey(LPCSTR lpcKeyName) {
  HKEY hSubKey;
  RegCreateKeyEx(hKey, lpcKeyName, NULL, NULL,
      REG_OPTION_VOLATILE, NULL, NULL,
      &hSubKey, NULL);
  return RegCloseKey(hSubKey);
}

LSTATUS Reg::deleteSubKey(LPCSTR lpcKeyName) {
  return RegDeleteKey(hKey, lpcKeyName);
}

LPSTR Reg::enumSubKeys (DWORD dwIndex) {
  DWORD dwSize = 255;
  LPSTR lptstr = new CHAR[dwSize];
  status = RegEnumKeyEx(hKey, dwIndex,
      lptstr, &dwSize, NULL, NULL, NULL, NULL);
    
  if (ERROR_SUCCESS != status) {
    return NULL;
  }
  return lptstr;
}

DWORD Reg::getCountOfSubKeys() {
  DWORD lpcCountOfSubKeys;
  RegQueryInfoKey(hKey, NULL, NULL, NULL,
    &lpcCountOfSubKeys,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  return lpcCountOfSubKeys;
}