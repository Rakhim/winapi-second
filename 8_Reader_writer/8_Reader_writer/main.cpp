#include <windows.h>
#include <stdio.h>
#include "GetLastError.h"

const DWORD nThreads = 2;
HANDLE hThreads[nThreads];
HANDLE hSemaphoreReader;
HANDLE hSemaphoreWriter;
HANDLE hEvent;
HANDLE hEventAndReader[2];
DWORD WINAPI threadFunctionReader(LPVOID lpParam);
DWORD WINAPI threadFunctionWriter(LPVOID lpParam);

int main(int argc, char **argv) {
  hSemaphoreReader = CreateSemaphore(NULL, 0, nThreads - 1, NULL);
  if (NULL == hSemaphoreReader) {
    ErrorExit("\CreateSemaphore(Reader)");
  }
  
  hSemaphoreWriter = CreateSemaphore(NULL, 1, 1, NULL);
  if (NULL == hSemaphoreWriter) {
    ErrorExit("\CreateSemaphore(Writer)");
  }
  
  hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  if (NULL == hEvent) {
    ErrorExit("\CreateEvent");
  }
  
  hEventAndReader[0] = hEvent;
  hEventAndReader[1] = hSemaphoreReader;  

  hThreads[0] = CreateThread(NULL, 0, threadFunctionWriter, NULL, CREATE_SUSPENDED, NULL);
  for (size_t i = 1; i < nThreads; i++) {
    hThreads[i] = CreateThread(NULL, 0, threadFunctionReader, NULL, CREATE_SUSPENDED, NULL);
    if (NULL == hThreads[i]) {
      ErrorExit("CreateThread");
    }
  }
  
  for (size_t i = 0; i < nThreads; i++) {
    if ((DWORD)(-1) == ResumeThread(hThreads[i])) {
      ErrorExit("ResumeThread");
    }
  }

  WaitForMultipleObjects(nThreads, hThreads, TRUE, INFINITE);

  for (size_t i = 0; i < nThreads; i++) {
    if (NULL == CloseHandle(hThreads[i])) {
      ErrorExit("CloseHandle(one of threads)");
    }
  }

  if (NULL == CloseHandle(hSemaphoreReader)) {
    ErrorExit("CloseHandle(Semaphore for Reader)");
  }
  if (NULL == CloseHandle(hSemaphoreWriter)) {
    ErrorExit("CloseHandle(Semaphore for Writer)");
  }
  printf("\n");
  system("pause");
  return 0;
}

DWORD WINAPI threadFunctionWriter(LPVOID lpParam) {
  for (size_t i = 0; TRUE; i++) {
    if (WAIT_OBJECT_0 == WaitForSingleObject(hSemaphoreWriter, INFINITE)) {      
      if (i > 20) {
        SetEvent(hEvent);
        ReleaseSemaphore(hSemaphoreReader, 1, NULL);
        return 0;     
      }
      printf("\nPing - ");
      ReleaseSemaphore(hSemaphoreReader, 1, NULL);
    }
  }
  return 0;
}

DWORD WINAPI threadFunctionReader(LPVOID lpParam) {
  for (size_t i = 0; TRUE ; i++) {
    DWORD dwWait = WaitForMultipleObjects(2, hEventAndReader, FALSE, INFINITE);
    if (WAIT_OBJECT_0 == dwWait) { 
      return 0;
    }
    printf("pong");
    ReleaseSemaphore(hSemaphoreWriter, 1, NULL);
  }
  return 0;
}