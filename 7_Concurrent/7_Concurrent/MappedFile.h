#pragma once
#include <windows.h>

#define MAP_SIZE (1 << 15)

struct MAPPED_FILE_EXCEPTION {
  char *message;
};


class MappedFile {
  HANDLE hFile;
  HANDLE hMap;
  DWORD dwMaximumSize;
  LPVOID lpBaseAddress;
  size_t countOfMaps;
  
public:
  MappedFile(LPCSTR lpFileName, DWORD dwMaximumSize);
  ~MappedFile(VOID);
  DWORD getDword(DWORD index);
  BOOL setDword(DWORD index, DWORD value);
  LPCWSTR getString(DWORD offsetInWchars, DWORD length);
  BOOL setString(DWORD offsetInWchars, LPCWSTR value, DWORD length);
  DWORD& operator[] (DWORD index);

private:
  VOID map(DWORD dwNumberOfBytesToMap);
  VOID unmap();
  size_t bytesToMaps(size_t bytes);
};
