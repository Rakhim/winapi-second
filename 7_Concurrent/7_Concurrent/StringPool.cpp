#include "StringPool.h"

StringPool::StringPool(LPCSTR lpFileName, LPCSTR lpFileNameOfIndexes):
  countOfStrings(0),
  hMutex(INVALID_HANDLE_VALUE)
{
  hMutex = CreateMutex(NULL, FALSE, STRING_POOL_MUTEX_NAME);
  if (NULL == hMutex) {
    STRING_POOL_EXCEPTION ex;
    ex.message = "CreateMutex";
    throw ex;
  }
  mf = new MappedFile(lpFileName, STRING_POOL_MAX_SIZE);
  mfIndexes = new MappedFile(lpFileNameOfIndexes, STRING_POOL_MAX_SIZE / (2 * sizeof(WCHAR)) * sizeof(DWORD));
}

StringPool::~StringPool(void) {
  CloseHandle(hMutex);
  delete mf;
  delete mfIndexes;
}

BOOL StringPool::addString(const LPCWSTR lpString, DWORD length) {
  if (!lpString || !length) {
    return FALSE;
  }

  DWORD sizeInBytes = sizeof(WCHAR) * (length + 1);
  if (STRING_POOL_MAX_STRING_SIZE < sizeInBytes) {
    return FALSE;
  }

  if (WAIT_OBJECT_0 == WaitForSingleObject(hMutex, INFINITE)) {  
    countOfStrings = mfIndexes->getDword(0);
    mfIndexes->setDword(0, 0);

    if (mfIndexes->getDword(countOfStrings) + sizeInBytes > STRING_POOL_MAX_SIZE) {
      STRING_POOL_EXCEPTION e;
      e.message = "EOF";
      throw e;
    }

    if (mf->setString(mfIndexes->getDword(countOfStrings), lpString, length)) {
      if (FALSE == mfIndexes->setDword(countOfStrings + 1, mfIndexes->getDword(countOfStrings) + length + 1)) {
        return FALSE;
      }
    } else {
      return FALSE;
    }
    ++countOfStrings;

    mfIndexes->setDword(0, countOfStrings);
    ReleaseMutex(hMutex);
  } else {
    return FALSE;
  }
  return TRUE;
}

DWORD StringPool::getPageSize() {
  SYSTEM_INFO si;
  GetSystemInfo(&si);
  return si.dwPageSize;
}

LPCWSTR StringPool::operator[](DWORD index) {
  LPCWSTR retValue = NULL;
  if (WAIT_OBJECT_0 == WaitForSingleObject(hMutex, INFINITE)) {
    countOfStrings = mfIndexes->getDword(0);
    mfIndexes->setDword(0, 0);
    retValue = (index < countOfStrings ?
      mf->getString(mfIndexes->getDword(index), STRING_POOL_MAX_STRING_SIZE) : NULL);
    mfIndexes->setDword(0, countOfStrings);
    ReleaseMutex(hMutex);
  }
  return retValue;
}

DWORD StringPool::getCount() {
  return countOfStrings;
}

DWORD StringPool::bytesToCountOfPages(DWORD bytes) {
  DWORD n = bytes / getPageSize();
  return (bytes % getPageSize() ? n + 1 : n);
}