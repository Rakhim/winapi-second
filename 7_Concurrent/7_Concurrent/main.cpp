#include "StringPool.h"
#include "GetLastError.h"
#include <iostream>

DWORD FilterFunction();

void check(int argc, char **argv) {
  try {
    size_t len = 0;
    LPWSTR string = NULL;

    if (argc < 2) {
      string = L"a";//L"default";
      len = 1;//7;
    } else {
      len = strlen(argv[1]);
      string = new WCHAR[len];
      MultiByteToWideChar(CP_NONE, 0, argv[1], len, string, len);
    }

    StringPool sp("pool", "poolIndexes");
    size_t i;
    size_t count = 10;
    for (i = 0; i < count && sp.addString(string, len); i++) {
      Sleep(15);
    }
    Sleep(15 * count);
    
    printf("\n size : %d \n stopped index : %d \n totally iterations: %d", sp.getCount(), i, count);
    system("pause");
  } catch (MAPPED_FILE_EXCEPTION e) {
    ErrorExit(e.message);
  } catch (STRING_POOL_EXCEPTION e) {
    printf("\n%s", e.message);
  }
}

int main(int argc, char **argv) {
  __try {
    check(argc, argv);
  } __except(FilterFunction()) {	
    
	  switch(GetExceptionCode()) {
		  case EXCEPTION_ACCESS_VIOLATION:
				  printf("\nEXCEPTION_ACCESS_VIOLATION");
			  break;		
		  case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
				  printf("\nEXCEPTION_ARRAY_BOUNDS_EXCEEDED");
			  break;
		  case EXCEPTION_INT_DIVIDE_BY_ZERO:
				  printf("\nEXCEPTION_INT_DIVIDE_BY_ZERO");
			  break;
	  }
	}
//  printf("\n");
//  system("pause");
  return 0;
}

DWORD FilterFunction(){
  return EXCEPTION_EXECUTE_HANDLER; 
}