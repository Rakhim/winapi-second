#pragma once
#include <windows.h>
#include "MappedFile.h"

const DWORD STRING_POOL_MAX_SIZE = (1 << 28);	//256 Mb
const DWORD STRING_POOL_MAX_COMMIT_OVERFLOW = (1 << 16); // 64 Kb
const DWORD STRING_POOL_MAX_STRING_SIZE = (1 << 14); // 16Kb
#define STRING_POOL_MUTEX_NAME "STRINGPOOLMUTEXNAME"

struct STRING_POOL_EXCEPTION {
  char *message;
};

class StringPool {
  DWORD countOfStrings;
  MappedFile *mf;
  MappedFile *mfIndexes;
  HANDLE hMutex;

public:
  StringPool(LPCSTR lpFileName, LPCSTR lpFileNameOfIndexes);
  ~StringPool(void);
  BOOL addString(const LPCWSTR, SIZE_T);
  LPCWSTR operator [](DWORD);
  DWORD getCount();
private:
  DWORD getPageSize();
  DWORD bytesToCountOfPages(DWORD);
};