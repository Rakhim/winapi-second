#pragma once
#include <windows.h>

const DWORD STRING_POOL_MAX_SIZE = (1 << 28);	//256 Mb
const DWORD STRING_POOL_MAX_COMMIT_OVERFLOW = (1 << 16); // 64 Kb
const DWORD STRING_POOL_MAX_STRING_SIZE = (1 << 14); // 16Kb

struct STRING_POOL_EXCEPTION {
  char *message;
};

class StringPool {
public:
  StringPool(void);
  ~StringPool(void);
  BOOL addString(const LPCWSTR, SIZE_T);
  LPCWSTR operator [](DWORD);
  DWORD getCount();
private:
  DWORD getPageSize();
  DWORD bytesToCountOfPages(DWORD);
private:
  LPCWSTR pPool;
  DWORD *pIndexes;
  DWORD commitedPagesForIndexes;
  DWORD countOfStrings;
  DWORD last;
};