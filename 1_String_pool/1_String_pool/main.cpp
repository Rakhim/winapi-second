#include <iostream>
#include "StringPool.h"

int main(int argc, char **argv) {
  
  try {
    StringPool stringPool;
    size_t i;
    for (i = 0;i < (1 << 30);i++) {
      stringPool.addString(L"AB", 1);
    }

    printf("\n index: %d \nAdded: %d ", i, stringPool.getCount());

  } catch (STRING_POOL_EXCEPTION e) {
    printf("%s\n", e.message);
  }
  wprintf(L"\n");
  system("pause");
  return 0;
}