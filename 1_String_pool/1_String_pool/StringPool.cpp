#include "StringPool.h"
#include <iostream>

StringPool::StringPool(void):
  pPool(NULL),
  commitedPagesForIndexes(0),
  countOfStrings(0),
  pIndexes(NULL),
  last(0)
{
  pPool = (LPCWSTR)VirtualAlloc(NULL, STRING_POOL_MAX_SIZE, MEM_RESERVE, PAGE_READWRITE);
  if (!pPool) {
    STRING_POOL_EXCEPTION ex;
    ex.message = "Constructor : pool";
    throw ex;
  }
  
  pIndexes = (DWORD*)VirtualAlloc(NULL,
    STRING_POOL_MAX_SIZE / 2 * sizeof(DWORD), // 2^14 * sizeof(DWORD)
    MEM_RESERVE, PAGE_READWRITE);

  if (!pIndexes) {
    VirtualFree((LPVOID)pPool, 0, MEM_RELEASE);
    STRING_POOL_EXCEPTION ex;
    ex.message = "Constructor : Indexes";
    throw ex;
  }
}

StringPool::~StringPool(void) {
  if (!VirtualFree((LPVOID)pPool, 0, MEM_RELEASE) ) {    
    STRING_POOL_EXCEPTION ex;
    ex.message = "Destructor : Pool";
    throw ex;
  }

  if (!VirtualFree((LPVOID)pIndexes, 0, MEM_RELEASE) ) {    
    STRING_POOL_EXCEPTION ex;
    ex.message = "Destructor : Indexes";
    throw ex;
  }
}

BOOL StringPool::addString(const LPCWSTR lpString, DWORD size) {
  if (!lpString || !size) {
    return FALSE;
  }

  DWORD sizeInBytes = sizeof(WCHAR) * (size + 1);
  if (STRING_POOL_MAX_STRING_SIZE < sizeInBytes) {
    return FALSE;
  }

  if (!(((countOfStrings) * sizeof(DWORD)) % getPageSize())) {
    if (!VirtualAlloc((LPVOID)((char*)pIndexes + getPageSize() * commitedPagesForIndexes), 1, MEM_COMMIT, PAGE_READWRITE)) {
      return FALSE;
    }
    ++commitedPagesForIndexes;
  }

  pIndexes[countOfStrings] = last;

  if (getPageSize() - sizeInBytes % getPageSize() > STRING_POOL_MAX_COMMIT_OVERFLOW) {
    return FALSE;
  }

  if (bytesToCountOfPages(last + sizeInBytes) > bytesToCountOfPages(STRING_POOL_MAX_SIZE)) {
    return FALSE;
  }

  if (VirtualAlloc((LPVOID)((char*)pPool + pIndexes[countOfStrings]), sizeInBytes, MEM_COMMIT, PAGE_READWRITE)) {
    CopyMemory((LPVOID)((char*)pPool + pIndexes[countOfStrings]), lpString, sizeInBytes - sizeof(WCHAR));
  } else {
    return FALSE;
  }  
  
  last = pIndexes[countOfStrings] + sizeInBytes;

  ++countOfStrings;
  return TRUE;
}

DWORD StringPool::getPageSize() {
  SYSTEM_INFO si;
  GetSystemInfo(&si);
  return si.dwPageSize;
}

LPCWSTR StringPool::operator[](DWORD index) {  
  return (index < countOfStrings ? (LPCWSTR)((char*)pPool + pIndexes[index]) : NULL);
}

DWORD StringPool::getCount() {
  return countOfStrings;
}

DWORD StringPool::bytesToCountOfPages(DWORD bytes) {
  DWORD n = bytes / getPageSize();
  return (bytes % getPageSize() ? n + 1 : n);
}