#include "ConsoleWindow.h"

ConsoleWindow::ConsoleWindow(void) {
  hInput = GetStdHandle(STD_INPUT_HANDLE);
  if (INVALID_HANDLE_VALUE == hInput) {
    throw;
  }
  hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
  if (INVALID_HANDLE_VALUE == hOutput) {
    throw;
  }
  hError = GetStdHandle(STD_ERROR_HANDLE);
  if (INVALID_HANDLE_VALUE == hError) {
    throw;
  }
}

ConsoleWindow::~ConsoleWindow(void) {
}

BOOL ConsoleWindow::setTitle(LPTSTR title) {
  return SetConsoleTitle(title);
}

LPTSTR ConsoleWindow::getTitle() {
  LPTSTR lptTitle = new TCHAR[MAX_PATH];
  if (NULL == GetConsoleTitle(lptTitle, MAX_PATH)) {
    delete[] lptTitle; 
    return NULL;
  }
  return lptTitle;
}

BOOL ConsoleWindow::readInput(PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD numberOfEventsRead) {
  return ReadConsoleInput(hInput, lpBuffer, nLength, numberOfEventsRead);
}

BOOL ConsoleWindow::write(LPCVOID lpBuffer, DWORD nNumberOfCharsToWrite, LPDWORD lpNumberOfCharsWritten) {
  return WriteConsole(hOutput, lpBuffer, nNumberOfCharsToWrite, lpNumberOfCharsWritten, NULL);
}

BOOL ConsoleWindow::read(LPVOID lpBuffer, LPDWORD lpNumberOfCharsRead) {
  BOOL retValue = ReadConsole(hInput, lpBuffer, MAX_PATH * sizeof(TCHAR), lpNumberOfCharsRead, NULL);
  ((PTCHAR)lpBuffer)[*lpNumberOfCharsRead] = 0;
  return retValue;
}

BOOL ConsoleWindow::setCursorPosition(COORD cursorPosition) {
  return SetConsoleCursorPosition(hOutput, cursorPosition);
}

BOOL ConsoleWindow::setCursorInfo(const CONSOLE_CURSOR_INFO consoleCursorinfo) {
  return SetConsoleCursorInfo(hOutput, &consoleCursorinfo);
}

BOOL ConsoleWindow::getScreenBufferInfo(PCONSOLE_SCREEN_BUFFER_INFO pConsoleScreenBufferInfo) {
  return GetConsoleScreenBufferInfo(hOutput,  pConsoleScreenBufferInfo);
}

BOOL ConsoleWindow::setWindowInfo(BOOL bAbsolute, const SMALL_RECT consoleWindow) {
  return SetConsoleWindowInfo(hOutput, bAbsolute, &consoleWindow);
}

BOOL ConsoleWindow::setTextAttributes(WORD wAttributes) {
  return SetConsoleTextAttribute(hOutput, wAttributes);
}

BOOL ConsoleWindow::write(LPCVOID lpBuffer, DWORD nNumberOfCharsToWrite, WORD wAttributes, LPDWORD lpNumberOfCharsWritten) {
  
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  getScreenBufferInfo(&csbi);
  WORD oldAttributes = csbi.wAttributes;
  setTextAttributes(wAttributes);
  BOOL retValue = write(lpBuffer, nNumberOfCharsToWrite);
  setTextAttributes(oldAttributes);
  return retValue;
}

BOOL ConsoleWindow::writeInput(const PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsWritten) {
  return WriteConsoleInput(hOutput, lpBuffer,  nLength, lpNumberOfEventsWritten);
}

BOOL ConsoleWindow::peekInput(PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsRead) {
  return PeekConsoleInput(hInput, lpBuffer, nLength, lpNumberOfEventsRead);
}

#include <stdio.h>
BOOL ConsoleWindow::keyboardKeyPressed() {
  INPUT_RECORD ir;
  DWORD nNumberOfEvents = 0;
  peekInput(&ir, 1, &nNumberOfEvents);
  if (nNumberOfEvents > 0 && KEY_EVENT == ir.EventType) {
    return TRUE;
  }
  return FALSE;
}

BOOL ConsoleWindow::mouseKeyPressed() {
  INPUT_RECORD ir;
  DWORD nNumberOfEvents = 0;
  peekInput(&ir, 1, &nNumberOfEvents);
  printf("! %d %d\n", ir.EventType, nNumberOfEvents);
  if (nNumberOfEvents > 0 && MOUSE_EVENT == ir.EventType) {
    printf("!\n");
    return TRUE;
  }
  return FALSE;
}

BOOL ConsoleWindow::setCursorPosition(SHORT x, SHORT y) {
  COORD coord;
  coord.X = x;
  coord.Y = y;
  return setCursorPosition(coord);
}