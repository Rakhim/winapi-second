#include "ConsoleWindow.h"

void keyEvent(ConsoleWindow &cw, INPUT_RECORD &ir, COORD &coord);
void mouseEvent(ConsoleWindow &cw, INPUT_RECORD &ir, COORD &food);
void addCoords(COORD &value, COORD shift);
void frame(ConsoleWindow &cw);

const size_t fieldRightMargin = 4;

int main(int argc, char **argv) {
  ConsoleWindow cw;
  cw.setTitle("Snake");

  BOOL b = TRUE;
  INPUT_RECORD ir;
  DWORD read;
  CHAR null;
  COORD coord = {1, 1};
  COORD shift = {1, 0};
  COORD food = {1, 1};
  DWORD length = 5;
  const size_t maxSnakeLength = 20;
  COORD snake[maxSnakeLength];
  snake[0].X = 1;
  snake[0].Y = 1;
  cw.setCursorPosition(snake[0]);
  for (size_t i = 1; i < length; i++) {
    snake[i].X = i;
    snake[i].Y = 2;
    cw.setCursorPosition(snake[i]);
  }

  CONSOLE_SCREEN_BUFFER_INFO csbi;
  
  frame(cw);
  size_t tale = 0;
//  size_t head = length - 1;
  CONSOLE_CURSOR_INFO cci;
  ZeroMemory(&cci, sizeof(CONSOLE_CURSOR_INFO));
  cci.dwSize = sizeof(CONSOLE_CURSOR_INFO);
  cci.bVisible = FALSE;
  cw.setCursorInfo(cci);
  while (b) {
    
    if (coord.X == food.X && coord.Y == food.Y) {
      ++length;
    }

    cw.setCursorPosition(snake[tale]);
    cw.write(" \b", 2);
    snake[tale] = coord;
    tale = (++tale % length);
    for (size_t i = 0; i < length; i++) {
      cw.setCursorPosition(snake[i]);
      cw.write("o\b", 2, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
    }
    read = 0;
    cw.peekInput(&ir, 1, &read);

    if (read > 0) {
      switch (ir.EventType) {
        case MOUSE_EVENT:
          mouseEvent(cw, ir, food);
          break;
        case KEY_EVENT:
          keyEvent(cw, ir, shift);
          break;
        default: {
          cw.readInput(&ir, 1, &read);
        }
      } // switch peek
    } // if peek

    addCoords(coord, shift);

    cw.getScreenBufferInfo(&csbi);
    
    coord.X = (coord.X < 1 ? csbi.srWindow.Right - fieldRightMargin - 1 : (coord.X > csbi.srWindow.Right - fieldRightMargin - 1 ? 1 : coord.X));
    coord.Y = (coord.Y < 1 ? csbi.srWindow.Bottom - fieldRightMargin : (coord.Y > csbi.srWindow.Bottom - 1 ? 1 : coord.Y));
    //cw.setCursorPosition(1, csbi.srWindow.Right - fieldRightMargin);
    //cw.write("legth", 5);
    //cw.setCursorPosition(1, csbi.srWindow.Right - fieldRightMargin);
    //cw.write(, 1);
     
    Sleep(70);
  } // base while
  return 0;
}

void keyEvent(ConsoleWindow &cw, INPUT_RECORD &ir, COORD &shift) {
  DWORD read;
  cw.readInput(&ir, 1, &read);
  switch (ir.Event.KeyEvent.wVirtualKeyCode) {
    case VK_DOWN:
      shift.X = 0;
      shift.Y = 1;
      break;
    case VK_UP:
      shift.X = 0;
      shift.Y = -1;
      break;
    case VK_LEFT:
      shift.X = -1;
      shift.Y = 0;
      break;
    case VK_RIGHT:
      shift.X = 1;
      shift.Y = 0;
      break;    
  }
}

void mouseEvent(ConsoleWindow &cw, INPUT_RECORD &ir, COORD &food) {
  DWORD read;
  cw.readInput(&ir, 1, &read);
  if (FROM_LEFT_1ST_BUTTON_PRESSED == ir.Event.MouseEvent.dwButtonState) {
    cw.setCursorPosition(food);
    cw.write(" \b", 2);
    food = ir.Event.MouseEvent.dwMousePosition;
    cw.setCursorPosition(food);
    cw.write("x\b", 2, FOREGROUND_GREEN | FOREGROUND_INTENSITY | BACKGROUND_BLUE);
  }  
}

void addCoords(COORD &value, COORD shift) {
  value.X += shift.X;
  value.Y += shift.Y;
}

void frame(ConsoleWindow &cw) {
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  cw.getScreenBufferInfo(&csbi);
  COORD pos;

  for (SHORT i = 1; i < csbi.srWindow.Right - fieldRightMargin; i++) {
    cw.setCursorPosition(i, 0);
    cw.write("-", 1);
    cw.setCursorPosition(i, csbi.srWindow.Bottom);
    cw.write("-", 1);
  }
  cw.write("\b", 1);
  for (SHORT i = 1; i < csbi.srWindow.Bottom; i++) {
    cw.setCursorPosition(0, i);
    cw.write("|", 1); 
    cw.setCursorPosition(csbi.srWindow.Right - fieldRightMargin, i);
    cw.write("|", 1);
  }
}