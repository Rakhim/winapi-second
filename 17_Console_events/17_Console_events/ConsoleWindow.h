#pragma once
#include <windows.h>

class ConsoleWindow {
  HANDLE hInput;
  HANDLE hOutput;
  HANDLE hError;
  
public:
  BOOL setTitle(LPTSTR title);
  LPTSTR getTitle();

  BOOL write(LPCVOID lpBuffer, DWORD nNumberOfCharsToWrite, LPDWORD lpNumberOfCharsWritten = NULL);
  BOOL write(LPCVOID lpBuffer, DWORD nNumberOfCharsToWrite, WORD wAttributes, LPDWORD lpNumberOfCharsWritten = NULL);
  BOOL writeInput(const PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsWritten = NULL);

  BOOL read(LPVOID lpBuffer, LPDWORD lpNumberOfCharsRead);
  BOOL readInput(PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD numberOfEventsRead);
  BOOL peekInput(PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsRead);

  BOOL keyboardKeyPressed();
  BOOL mouseKeyPressed();

  BOOL setCursorPosition(SHORT x, SHORT y);
  BOOL setCursorPosition(COORD cursorPosition);
  BOOL setCursorInfo(const CONSOLE_CURSOR_INFO consoleCursorinfo);

  BOOL getScreenBufferInfo(PCONSOLE_SCREEN_BUFFER_INFO pConsoleScreenBufferInfo);

  BOOL setWindowInfo(BOOL bAbsolute, const SMALL_RECT consoleWindow);
  BOOL setTextAttributes(WORD wAttributes);

  ConsoleWindow(void);
  ~ConsoleWindow(void);
};