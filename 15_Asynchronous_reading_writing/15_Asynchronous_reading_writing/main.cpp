#include <Windows.h>
#include <stdio.h>

int main (int argc, char **argv) {
  HANDLE hFile = CreateFile("ouput.txt",
	GENERIC_ALL,
	0,
	NULL,
	CREATE_ALWAYS,
	FILE_FLAG_OVERLAPPED,
	NULL); 

  if (INVALID_HANDLE_VALUE == hFile) {
    printf("\n Error code : %d", GetLastError());
    system("pause");
    return 0;
  }
  // writing
  const int numberOfEvents = 2;
  HANDLE hEvents[numberOfEvents];

  hEvents[0] = CreateEvent(NULL, FALSE, FALSE, NULL);
  hEvents[1] = CreateEvent(NULL, FALSE, FALSE, NULL);

  OVERLAPPED overlaps[numberOfEvents];
  
  ZeroMemory(&overlaps[0], sizeof(overlaps[0]));
  ZeroMemory(&overlaps[1], sizeof(overlaps[1]));

  overlaps[0].hEvent = hEvents[0];
  overlaps[1].hEvent = hEvents[1];

  char first[] = "first longest string ";
  char second[] = "second string";

  WriteFile(hFile, first, strlen(first), NULL, &overlaps[0]);
  overlaps[1].Offset = strlen(first);
  WriteFile(hFile, second, strlen(second), NULL, &overlaps[1]);
	
  if (WAIT_OBJECT_0 == WaitForMultipleObjects(numberOfEvents, hEvents, TRUE, INFINITE)){
	  printf(" Writing is completed.\n");
  }
  
  CloseHandle(hEvents[0]);
  CloseHandle(hEvents[1]);

  // reading
  hEvents[0] = CreateEvent(NULL, FALSE, FALSE, NULL);
  hEvents[1] = CreateEvent(NULL, FALSE, FALSE, NULL);

  ZeroMemory(&overlaps[0], sizeof(overlaps[0]));
  ZeroMemory(&overlaps[1], sizeof(overlaps[1]));

  overlaps[0].hEvent = hEvents[0];
  overlaps[1].hEvent = hEvents[1];

  char *firstRead = new char[strlen(first) + 1];
  char *secondRead = new char[strlen(second) + 1];

  ReadFile(hFile, firstRead, strlen(first), NULL, &overlaps[0]);
  overlaps[1].Offset = strlen(first);
  firstRead[strlen(first)] = 0;
  ReadFile(hFile, secondRead, strlen(second), NULL, &overlaps[1]);
  secondRead[strlen(second)] = 0;

  if (WAIT_OBJECT_0 == WaitForMultipleObjects(numberOfEvents, hEvents, TRUE, INFINITE)){
	  printf("\n Reading is completed. \n %s\n %s\n", firstRead, secondRead);
	  delete[] firstRead;
	  delete[] secondRead;
  }

  CloseHandle(hFile);
  CloseHandle(hEvents[0]);
  CloseHandle(hEvents[1]);
  system("pause");
  return 0;
}